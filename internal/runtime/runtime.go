package runtime

import (
	"unsafe"
)

type Interface struct {
	Type *Type
	Data unsafe.Pointer
}

type String struct {
	Data unsafe.Pointer
	Len  int
}

type Type struct {
	size   uintptr
	_      uintptr
	_      uint32
	flag   uint8
	_      [3]uint8
	_      [2]uintptr
	strOff int32
	_      int32
}

type PointerType struct {
	Type
	Elem *Type
}

func (typ *Type) String() string {
	n := resolveNameOff(unsafe.Pointer(typ), typ.strOff)
	if n == nil {
		return ""
	}

	var i, l int
	var x byte
	for {
		x = *(*byte)(unsafe.Add(unsafe.Pointer(n), 1))
		l += int(x&0x7f) << (7 * i)
		i++
		if x&0x80 == 0 {
			break
		}
	}

	s := *(*string)(unsafe.Pointer(&String{
		Data: unsafe.Add(unsafe.Pointer(n), i+1),
		Len:  l,
	}))

	if typ.flag&2 != 0 {
		return s[1:]
	}

	return s
}

//go:linkname resolveNameOff runtime.resolveNameOff
func resolveNameOff(_ unsafe.Pointer, _ int32) *byte
