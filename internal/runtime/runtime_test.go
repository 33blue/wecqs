package runtime

import (
	"reflect"
	"testing"
	"unsafe"
)

type TestStruct struct{}

func TestType_String(t *testing.T) {
	tests := map[any]string{
		"":           "string",
		int(0):       "int",
		int8(0):      "int8",
		int16(0):     "int16",
		int32(0):     "int32",
		int64(0):     "int64",
		uint(0):      "uint",
		uint8(0):     "uint8",
		uint16(0):    "uint16",
		uint32(0):    "uint32",
		uint64(0):    "uint64",
		float32(0):   "float32",
		float64(0):   "float64",
		TestStruct{}: "runtime.TestStruct",

		new(string):     "*string",
		new(int):        "*int",
		new(int8):       "*int8",
		new(int16):      "*int16",
		new(int32):      "*int32",
		new(int64):      "*int64",
		new(uint):       "*uint",
		new(uint8):      "*uint8",
		new(uint16):     "*uint16",
		new(uint32):     "*uint32",
		new(uint64):     "*uint64",
		new(float32):    "*float32",
		new(float64):    "*float64",
		new(TestStruct): "*runtime.TestStruct",

		[1]string{}:     "[1]string",
		[1]int{}:        "[1]int",
		[1]int8{}:       "[1]int8",
		[1]int16{}:      "[1]int16",
		[1]int32{}:      "[1]int32",
		[1]int64{}:      "[1]int64",
		[1]uint{}:       "[1]uint",
		[1]uint8{}:      "[1]uint8",
		[1]uint16{}:     "[1]uint16",
		[1]uint32{}:     "[1]uint32",
		[1]uint64{}:     "[1]uint64",
		[1]float32{}:    "[1]float32",
		[1]float64{}:    "[1]float64",
		[1]TestStruct{}: "[1]runtime.TestStruct",

		new([]string):     "*[]string",
		new([]int):        "*[]int",
		new([]int8):       "*[]int8",
		new([]int16):      "*[]int16",
		new([]int32):      "*[]int32",
		new([]int64):      "*[]int64",
		new([]uint):       "*[]uint",
		new([]uint8):      "*[]uint8",
		new([]uint16):     "*[]uint16",
		new([]uint32):     "*[]uint32",
		new([]uint64):     "*[]uint64",
		new([]float32):    "*[]float32",
		new([]float64):    "*[]float64",
		new([]TestStruct): "*[]runtime.TestStruct",
	}

	for k, v := range tests {
		iface := (*Interface)(unsafe.Pointer(&k))
		t.Logf("%s == %s ?", iface.Type.String(), v)
		if iface.Type.String() != v {
			t.Error("false")
		}
	}
}

func BenchmarkType_String(b *testing.B) {
	s := new(TestStruct)

	b.Run("reflect", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = reflect.TypeOf(s).String()
		}
	})

	b.Run("runtime", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = (*Interface)(unsafe.Pointer(&s)).Type.String()
		}
	})
}
