package wecqs

import (
	"errors"
	"reflect"
	"unsafe"

	"github.com/goccy/go-json"

	"gitlab.com/33blue/wecqs/internal/runtime"
)

type componentData struct {
	Name string          `json:"name"`
	Data json.RawMessage `json:"data"`
}

// NewEntityFromJSON creates and returns a new entity from raw JSON data.
func NewEntityFromJSON(raw []byte) (*Entity, error) {
	entity := new(Entity)
	err := entity.UnmarshalJSON(raw)
	return entity, err
}

// MarshalJSON returns the entities components as raw JSON data.
func (entity *Entity) MarshalJSON() (raw []byte, err error) {
	if entity == nil {
		return nil, errors.New("entity is nil")
	}

	componentDatas := make([]componentData, 0, entity.components.Len())
	entity.components.Scan(func(_ uintptr, component any) bool {
		raw, err = json.Marshal(component)
		if err != nil {
			return false
		}

		componentDatas = append(componentDatas, componentData{
			Name: reflect.TypeOf(component).String(),
			Data: raw,
		})

		return true
	})
	if err != nil {
		return nil, err
	}

	return json.Marshal(componentDatas)
}

// UnmarshalJSON builds an entity from raw JSON data.
//
// The components present in the JSON data must have been previously registered to the component registry, either
// automatically through entities created with NewEntity, or manually using RegisterComponent.
func (entity *Entity) UnmarshalJSON(raw []byte) error {
	if entity == nil {
		return errors.New("entity is nil")
	}

	entity.components.Clear()

	var datas []componentData
	err := json.Unmarshal(raw, &datas)
	if err != nil {
		return err
	}

	for _, data := range datas {
		component, err := newComponentFromJSON(data.Name, data.Data)
		if err != nil {
			return err
		}

		header := (*runtime.Interface)(unsafe.Pointer(&component))
		entity.components.Set(uintptr(unsafe.Pointer(header.Type)), component)
	}

	return nil
}
