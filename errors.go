package wecqs

import (
	"errors"
	"strings"

	"golang.org/x/exp/maps"
)

type ErrInputsConflict struct{ ConflictsWith map[string]*Input }

func (e ErrInputsConflict) Error() string {
	return "new input mapping would conflict with existing mappings: " + strings.Join(maps.Keys(e.ConflictsWith), ", ")
}

var (
	// ErrShutdown is an error which can be returned by an UpdateSystem to gracefully stop the game.
	ErrShutdown = errors.New("game shutdown")

	ErrWorldUnavailable = errors.New("world is not available")
	ErrWorldNoneActive  = errors.New("no world is currently active")

	ErrSceneUnavailable = errors.New("scene is not available")
	ErrSceneNoneActive  = errors.New("no scene is currently active")

	ErrInputsUnavailable    = errors.New("input mapping is not available")
	ErrInputsRebindDisabled = errors.New("input mapping has rebinding disabled")

	ErrAssetUnavailable = errors.New("asset is not available")
	ErrExpectedFolder   = errors.New("expected path to folder")
	ErrUnhandledType    = errors.New("unhandled type")
)
