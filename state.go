package wecqs

import (
	"archive/zip"
	_ "embed"
	"image"
	"io/fs"
	"sync"

	ebitengine "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/audio"
)

// State stores the current game state.
type State struct {
	settings stateSettings
	assets   stateAssets
	inputs   stateInputs
	canvas   *ebitengine.Image
	worlds   map[string]*World
	audio    *audio.Context

	world *World
	scene *Scene

	nextWorld string
	nextScene string

	entitiesToAdd    []*Entity
	entitiesToRemove []*Entity

	queries sync.WaitGroup
	cancels []func()
}

type stateSettings struct {
	vsync           bool
	allowResize     bool
	allowFullscreen bool
	allowHighDPI    bool

	// canvas size and scale.
	canvasWidth  int
	canvasHeight int
	canvasScale  int

	// canvas size limits.
	minWidth  int
	minHeight int
	maxWidth  int
	maxHeight int

	// tps limit.
	maxTPS int
}

func newState() *State {
	return &State{
		settings: stateSettings{
			vsync:           true,
			allowFullscreen: true,
			allowHighDPI:    true,
			canvasWidth:     640,
			canvasHeight:    480,
			canvasScale:     1,
			minWidth:        -1,
			minHeight:       -1,
			maxWidth:        -1,
			maxHeight:       -1,
			maxTPS:          60,
		},
		assets: stateAssets{assetReaders: map[string]AssetReader{
			".zip": func(path string) (fs.FS, error) { return zip.OpenReader(path) },
		}},
		inputs: stateInputs{inputs: make(map[string]*Input)},
		worlds: make(map[string]*World),
	}
}

// SetVsync sets if vsync is enabled or disabled.
func (state *State) SetVsync(enable bool) {
	LogDebug("set vsync: %t", enable)

	// note: SetFPSMode is getting deprecated at some point and SetVsyncEnabled is getting un-deprecated.
	// see https://github.com/hajimehoshi/ebiten/issues/2338.
	ebitengine.SetVsyncEnabled(enable)
}

// SetFullscreen sets if the game should be fullscreen. This does not have an effect on macOS if fullscreen has been
// enabled natively by the desktop.
//
// This has no effect when running on non-desktops.
func (state *State) SetFullscreen(enable bool) {
	LogDebug("set fullscreen: %t", enable)
	ebitengine.SetFullscreen(true)
}

// SetResolution sets the resolution of the game.
//
// This has no effect when running on non-desktops.
func (state *State) SetResolution(width, height int) {
	LogDebug("set resolution: %dx%d", width, height)
	ebitengine.SetWindowSize(width*state.settings.canvasScale, height*state.settings.canvasScale)
}

// Switch sets the next world and scene to switch to.
func (state *State) Switch(worldName, sceneName string) {
	state.nextWorld = worldName
	state.nextScene = sceneName
}

// CanvasSize returns the current canvas size.
func (state *State) CanvasSize() (int, int) {
	return state.settings.canvasWidth, state.settings.canvasHeight
}

// CanvasScale returns the current canvas scale.
func (state *State) CanvasScale() int {
	return state.settings.canvasScale
}

// AddEntities queues up the given entities to be added to the current scene at the end of the current OnUpdate cycle.
func (state *State) AddEntities(entities ...*Entity) {
	state.entitiesToAdd = append(state.entitiesToAdd, entities...)
}

// RemoveEntities queues up the entities with the given IDs to be removed from the current scene at the end of the
// current OnUpdate cycle.
func (state *State) RemoveEntities(entities ...*Entity) {
	state.entitiesToRemove = append(state.entitiesToRemove, entities...)
}

// addWorlds adds all the given worlds to the game state.
func (state *State) addWorlds(worlds ...*World) {
	for _, world := range worlds {
		LogDebug("add world %s to state", world.name)
		state.worlds[world.name] = world
	}
}

// update loop.
func (state *State) update() (err error) {
	if state.world == nil {
		return ErrWorldNoneActive
	}

	if state.scene == nil {
		return ErrSceneNoneActive
	}

	// run systems.
	state.world.systems.Scan(func(_ int, system any) bool {
		if updateSystem, ok := system.(UpdateSystem); ok {
			err = updateSystem.OnUpdate(state)
		}

		return err == nil
	})

	// add/remove entities.
	state.applyEntities()

	// switch scene/world.
	return state.switchActive()
}

// draw loop.
func (state *State) draw(screen *ebitengine.Image) {
	state.canvas.Clear()

	// draw nothing if there is no active world or scene.
	// this method can't return an error, but update can.
	if state.world == nil || state.scene == nil {
		return
	}

	// run every DrawSystem in this world.
	state.world.systems.Scan(func(_ int, system any) bool {
		if drawSystem, ok := system.(DrawSystem); ok {
			drawSystem.OnDraw(state, state.canvas)
		}

		return true
	})

	// add/remove entities.
	state.applyEntities()

	// draw canvas to screen.
	screen.DrawImage(state.canvas, nil)
}

// layout loop.
func (state *State) layout(screenWidth, screenHeight int) (int, int) {
	// scale canvas if high-DPI-aware mode enabled.
	if state.settings.allowHighDPI {
		scaleFactor := ebitengine.DeviceScaleFactor()
		screenWidth = int(float64(screenWidth) * scaleFactor)
		screenHeight = int(float64(screenHeight) * scaleFactor)
	}

	// check if canvas has changed size since last run.
	if state.canvas != nil && state.canvas.Bounds().Size() == image.Pt(screenWidth, screenHeight) {
		return screenWidth, screenHeight
	}

	// initialise new canvas.
	canvas := ebitengine.NewImage(screenWidth/state.settings.canvasScale, screenHeight/state.settings.canvasScale)

	// run every LayoutSystem in this world.
	state.world.systems.Scan(func(_ int, system any) bool {
		if layoutSystem, ok := system.(LayoutSystem); ok {
			layoutSystem.OnLayout(state, state.canvas)
		}

		return true
	})

	// add/remove entities.
	state.applyEntities()

	// set and return canvas size.
	state.canvas = canvas
	state.settings.canvasWidth, state.settings.canvasHeight = state.canvas.Size()
	return state.settings.canvasWidth, state.settings.canvasHeight
}

// switchActive switches the currently active world and/or scene.
func (state *State) switchActive() (err error) {
	switchWorld := state.nextWorld != ""

	if switchWorld || state.nextScene != "" {
		var ok bool

		// unload current world (if applicable).
		if switchWorld {
			if state.world != nil {
				err = state.unloadWorld()
				if err != nil {
					return err
				}
			}
		}

		// unload current scene.
		if state.scene != nil {
			err = state.unloadScene()
			if err != nil {
				return err
			}
		}

		// switch world.
		lastWorld := state.world
		if switchWorld {
			state.world, ok = state.worlds[state.nextWorld]
			if !ok {
				return ErrWorldUnavailable
			}
		}

		// switch scene.
		state.scene, ok = state.world.scenes[state.nextScene]
		if !ok {
			return ErrSceneUnavailable
		}

		// reload assets if needed.
		if lastWorld == nil || state.world.assets != lastWorld.assets {
			err = state.reloadAssets()
			if err != nil {
				return err
			}
		}

		// load new scene.
		err = state.loadScene()
		if err != nil {
			return err
		}

		// load new world (if applicable).
		if switchWorld {
			err = state.loadWorld()
			if err != nil {
				return err
			}
		}

		// clear names.
		state.nextWorld = ""
		state.nextScene = ""
	}

	return nil
}

func (state *State) loadWorld() (err error) {
	// run world loading systems.
	LogInfo("loading world %s", state.world.name)
	state.world.systems.Scan(func(_ int, system any) bool {
		if worldLoadSystem, ok := system.(WorldLoadSystem); ok {
			err = worldLoadSystem.OnWorldLoad(state, state.world)
		}

		return err == nil
	})

	// add/remove entities.
	state.applyEntities()

	return err
}

func (state *State) unloadWorld() (err error) {
	// run unloading world systems.
	LogInfo("unloading world %s", state.world.name)
	state.world.systems.Scan(func(_ int, system any) bool {
		if worldLoadSystem, ok := system.(WorldUnloadSystem); ok {
			err = worldLoadSystem.OnWorldUnload(state, state.world)
		}

		return err == nil
	})

	// add/remove entities.
	state.applyEntities()

	return err
}

func (state *State) loadScene() (err error) {
	// run loading scene systems.
	LogInfo("loading scene %s", state.scene.name)
	state.world.systems.Scan(func(_ int, system any) bool {
		if worldLoadSystem, ok := system.(SceneLoadSystem); ok {
			err = worldLoadSystem.OnSceneLoad(state, state.scene)
		}

		return err == nil
	})

	// add/remove entities.
	state.applyEntities()

	return err
}

func (state *State) unloadScene() (err error) {
	// run unloading scene systems.
	LogInfo("unloading scene %s", state.scene.name)
	state.world.systems.Scan(func(_ int, system any) bool {
		if worldLoadSystem, ok := system.(SceneUnloadSystem); ok {
			err = worldLoadSystem.OnSceneUnload(state, state.scene)
		}

		return err == nil
	})

	// add/remove entities.
	state.applyEntities()

	return err
}

func (state *State) applyEntities() {
	// cancel remaining queries
	for _, cancel := range state.cancels {
		cancel()
	}

	// clear cancel slice (leave underlying capacity allocated).
	state.cancels = state.cancels[:0]

	// wait for queries to finish.
	state.queries.Wait()

	// remove entities.
	if len(state.entitiesToRemove) > 0 {
		state.scene.removeEntities(state.entitiesToRemove...)
		state.entitiesToRemove = nil
	}

	// add entities.
	if len(state.entitiesToAdd) > 0 {
		state.scene.addEntities(state.entitiesToAdd...)
		state.entitiesToAdd = nil
	}
}
