package wecqs

import (
	"bufio"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/fatih/color"
)

type Logger interface {
	Trace(layout string, v ...any)
	Debug(layout string, v ...any)
	Info(layout string, v ...any)
	Warn(layout string, v ...any)
	Error(layout string, v ...any)
}

// log is the global package and state logger.
var log Logger = SilentLogger{}

// SetLogger sets the global package logger. The default value is SilentLogger, which does not log anything. Passing nil
// defaults the logger back to SilentLogger.
func SetLogger(logger Logger) {
	if logger == nil {
		log = SilentLogger{}
	} else {
		log = logger
	}
}

func LogTrace(layout string, v ...any) { log.Trace(layout, v...) }
func LogDebug(layout string, v ...any) { log.Debug(layout, v...) }
func LogInfo(layout string, v ...any)  { log.Info(layout, v...) }
func LogWarn(layout string, v ...any)  { log.Warn(layout, v...) }
func LogError(layout string, v ...any) { log.Error(layout, v...) }

// SilentLogger is a levelled logger implementation which does nothing.
type SilentLogger struct{}

func (s SilentLogger) Trace(string, ...any) {}
func (s SilentLogger) Debug(string, ...any) {}
func (s SilentLogger) Info(string, ...any)  {}
func (s SilentLogger) Warn(string, ...any)  {}
func (s SilentLogger) Error(string, ...any) {}

const (
	SimpleLoggerLevelNone = iota - 1
	SimpleLoggerLevelError
	SimpleLoggerLevelWarn
	SimpleLoggerLevelInfo
	SimpleLoggerLevelDebug
	SimpleLoggerLevelTrace
)

// SimpleLogger is a simple levelled logger implementation, which prints all logs to the console (stderr).
type SimpleLogger struct {
	// Level specifies what sort of logs should be outputted.
	Level int

	writer *bufio.Writer
	once   sync.Once
}

func (l *SimpleLogger) Trace(layout string, v ...any) {
	if l.Level >= 4 {
		l.once.Do(l.init)

		_, _ = fmt.Fprint(l.writer, time.Now().UnixNano())
		_ = l.writer.WriteByte(' ')
		_, _ = fmt.Fprint(l.writer, color.HiBlackString("TRC: "))
		_, _ = fmt.Fprintf(l.writer, layout, v...)
		_ = l.writer.WriteByte('\n')
		_ = l.writer.Flush()
	}
}

func (l *SimpleLogger) Debug(layout string, v ...any) {
	if l.Level >= 3 {
		l.once.Do(l.init)

		_, _ = fmt.Fprint(l.writer, time.Now().UnixNano())
		_ = l.writer.WriteByte(' ')
		_, _ = fmt.Fprint(l.writer, color.CyanString("DBG: "))
		_, _ = fmt.Fprintf(l.writer, layout, v...)
		_ = l.writer.WriteByte('\n')
		_ = l.writer.Flush()
	}
}

func (l *SimpleLogger) Info(layout string, v ...any) {
	if l.Level >= 2 {
		l.once.Do(l.init)

		_, _ = fmt.Fprint(l.writer, time.Now().UnixNano())
		_ = l.writer.WriteByte(' ')
		_, _ = fmt.Fprint(l.writer, color.HiGreenString("INF: "))
		_, _ = fmt.Fprintf(l.writer, layout, v...)
		_ = l.writer.WriteByte('\n')
		_ = l.writer.Flush()
	}
}

func (l *SimpleLogger) Warn(layout string, v ...any) {
	if l.Level >= 1 {
		l.once.Do(l.init)

		_, _ = fmt.Fprint(l.writer, time.Now().UnixNano())
		_ = l.writer.WriteByte(' ')
		_, _ = fmt.Fprint(l.writer, color.HiYellowString("WRN: "))
		_, _ = fmt.Fprintf(l.writer, layout, v...)
		_ = l.writer.WriteByte('\n')
		_ = l.writer.Flush()
	}
}

func (l *SimpleLogger) Error(layout string, v ...any) {
	if l.Level >= 0 {
		l.once.Do(l.init)

		_, _ = fmt.Fprint(l.writer, time.Now().UnixNano())
		_ = l.writer.WriteByte(' ')
		_, _ = fmt.Fprint(l.writer, color.HiRedString("ERR: "))
		_, _ = fmt.Fprintf(l.writer, layout, v...)
		_ = l.writer.WriteByte('\n')
		_ = l.writer.Flush()
	}
}

func (l *SimpleLogger) init() {
	l.writer = bufio.NewWriter(os.Stderr)
}

func quote(s string) string {
	return `"` + s + `"`
}
