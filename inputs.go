package wecqs

import (
	ebitengine "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

type inputMode int

const (
	InputKeyboard inputMode = iota
	InputMouseButton
	InputGamepadButton
	InputGamepadAxis
)

type Input struct {
	Mode inputMode

	KeyPrimary    ebitengine.Key
	KeySecondary  ebitengine.Key
	MouseButton   ebitengine.MouseButton
	GamepadID     ebitengine.GamepadID
	GamepadButton ebitengine.StandardGamepadButton
	GamepadAxis   ebitengine.StandardGamepadAxis

	disableRebind bool
}

// NewInput returns a new Input with all the input values set to -1 by default, meaning it will not react to any inputs.
func NewInput() *Input {
	return &Input{
		Mode:          -1,
		KeyPrimary:    -1,
		KeySecondary:  -1,
		MouseButton:   -1,
		GamepadID:     -1,
		GamepadButton: -1,
		GamepadAxis:   -1,
	}
}

// AddMode sets the input mode and returns the input.
func (input *Input) AddMode(mode inputMode) *Input {
	input.Mode = mode
	return input
}

// AddKeys sets the input keys and returns the input.
// If more than 2 keys are given, the rest are ignored.
func (input *Input) AddKeys(keys ...ebitengine.Key) *Input {
	if input.Mode == -1 {
		input.Mode = InputKeyboard
	}

	switch len(keys) {
	case 0:
		return input
	case 1:
		input.KeyPrimary = keys[0]
		return input
	default:
		input.KeyPrimary = keys[0]
		input.KeySecondary = keys[1]
		return input
	}
}

func (input *Input) AddMouseButton(mouseButton ebitengine.MouseButton) *Input {
	if input.Mode == -1 {
		input.Mode = InputMouseButton
	}
	input.MouseButton = mouseButton
	return input
}

func (input *Input) AddGamepadID(gamepadID ebitengine.GamepadID) *Input {
	input.GamepadID = gamepadID
	return input
}

func (input *Input) AddGamepadButton(gamepadButton ebitengine.StandardGamepadButton) *Input {
	if input.Mode == -1 {
		input.Mode = InputGamepadButton
	}
	input.GamepadButton = gamepadButton
	return input
}

func (input *Input) AddGamepadAxis(gamepadAxis ebitengine.StandardGamepadAxis) *Input {
	if input.Mode == -1 {
		input.Mode = InputGamepadAxis
	}
	input.GamepadAxis = gamepadAxis
	return input
}

func (input *Input) DisableRebind() *Input {
	input.disableRebind = true
	return input
}

func (input *Input) ModeAvailable() bool {
	switch input.Mode {
	case InputKeyboard, InputMouseButton:
		return true
	case InputGamepadAxis, InputGamepadButton:
		return ebitengine.IsStandardGamepadLayoutAvailable(input.GamepadID)
	default:
		return false
	}
}

func (input *Input) IsPressed() bool {
	switch input.Mode {
	case InputKeyboard:
		return doWithKeys(input, ebitengine.IsKeyPressed, func(a, b ebitengine.Key) bool {
			return ebitengine.IsKeyPressed(a) || ebitengine.IsKeyPressed(b)
		})
	case InputMouseButton:
		return ebitengine.IsMouseButtonPressed(input.MouseButton)
	case InputGamepadButton:
		return ebitengine.IsStandardGamepadButtonPressed(input.GamepadID, input.GamepadButton)
	case InputGamepadAxis:
		if ebitengine.StandardGamepadAxisValue(input.GamepadID, input.GamepadAxis) == 1.0 {
			return true
		}
		return false
	default:
		return false
	}
}

func (input *Input) IsJustPressed() bool {
	switch input.Mode {
	case InputKeyboard:
		return doWithKeys(input, inpututil.IsKeyJustPressed, func(a, b ebitengine.Key) bool {
			return inpututil.IsKeyJustPressed(a) || inpututil.IsKeyJustPressed(b)
		})
	case InputMouseButton:
		return inpututil.IsMouseButtonJustPressed(input.MouseButton)
	case InputGamepadButton:
		return inpututil.IsStandardGamepadButtonJustPressed(input.GamepadID, input.GamepadButton)
	// TODO: handle for InputGamepadAxis.
	default:
		return false
	}
}

func (input *Input) IsJustReleased() bool {
	switch input.Mode {
	case InputKeyboard:
		return doWithKeys(input, inpututil.IsKeyJustReleased, func(a, b ebitengine.Key) bool {
			return inpututil.IsKeyJustReleased(a) || inpututil.IsKeyJustReleased(b)
		})
	case InputMouseButton:
		return inpututil.IsMouseButtonJustReleased(input.MouseButton)
	case InputGamepadButton:
		return inpututil.IsStandardGamepadButtonJustReleased(input.GamepadID, input.GamepadButton)
	// TODO: handle for InputGamepadAxis
	default:
		return false
	}
}

func (input *Input) Value() float64 {
	switch input.Mode {
	case InputKeyboard:
		return doWithKeys(
			input,
			func(a ebitengine.Key) float64 {
				if ebitengine.IsKeyPressed(a) {
					return 1.0
				}
				return 0.0
			},
			func(a, b ebitengine.Key) float64 {
				if ebitengine.IsKeyPressed(a) || ebitengine.IsKeyPressed(b) {
					return 1.0
				}
				return 0.0
			},
		)
	case InputMouseButton:
		if ebitengine.IsMouseButtonPressed(input.MouseButton) {
			return 1.0
		}
		return 0.0
	case InputGamepadButton:
		return ebitengine.StandardGamepadButtonValue(input.GamepadID, input.GamepadButton)
	case InputGamepadAxis:
		return ebitengine.StandardGamepadAxisValue(input.GamepadID, input.GamepadAxis)
	default:
		return 0.0
	}
}

func (input *Input) PressDuration() int {
	switch input.Mode {
	case InputKeyboard:
		return doWithKeys(input, inpututil.KeyPressDuration, func(a, b ebitengine.Key) int {
			i, j := inpututil.KeyPressDuration(a), inpututil.KeyPressDuration(b)
			if i > j {
				return i
			}
			return j
		})
	case InputMouseButton:
		return inpututil.MouseButtonPressDuration(input.MouseButton)
	case InputGamepadButton:
		return inpututil.StandardGamepadButtonPressDuration(input.GamepadID, input.GamepadButton)
	default:
		return 0
	}
}

func doWithKeys[T any](input *Input, fnOne func(ebitengine.Key) T, fnBoth func(ebitengine.Key, ebitengine.Key) T) (t T) {
	if input.KeyPrimary > -1 && input.KeySecondary < 0 {
		return fnOne(input.KeyPrimary)
	}

	if input.KeyPrimary < 0 && input.KeySecondary > -1 {
		return fnOne(input.KeySecondary)
	}

	if input.KeyPrimary < 0 && input.KeySecondary < 0 {
		return
	}

	return fnBoth(input.KeyPrimary, input.KeySecondary)
}
