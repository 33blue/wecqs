package wecqs

import (
	"unsafe"

	"github.com/tidwall/btree"
)

type Scene struct {
	name string

	entities            []*Entity
	entityIndices       btree.Map[uintptr, int]
	entitiesByComponent btree.Map[uintptr, *btree.Map[uintptr, struct{}]]
}

// NewScene creates a new Scene with the given name and entities.
func NewScene(name string, entities ...*Entity) *Scene {
	log.Trace("new scene: %s", name)

	scene := &Scene{name: name}
	scene.addEntities(entities...)

	return scene
}

// Name returns the scene name.
func (scene *Scene) Name() string {
	return scene.name
}

// String returns the scene name.
// This is here to satisfy fmt.Stringer.
func (scene *Scene) String() string {
	return scene.name
}

// addEntities adds all the given entities to the scene.
func (scene *Scene) addEntities(entities ...*Entity) {
	log.Trace("add %d entities", len(entities))

	i := len(scene.entities)
	for _, entity := range entities {
		ptr := (uintptr)(unsafe.Pointer(entity))
		_, ok := scene.entityIndices.Get(ptr)
		if ok {
			continue
		}

		// append to entities slice.
		scene.entities = append(scene.entities, entity)
		scene.entityIndices.Set(ptr, i)
		i++

		// add to components index.
		entity.components.Scan(func(typ uintptr, _ any) bool {
			ents, ok := scene.entitiesByComponent.Get(typ)
			if !ok {
				ents = new(btree.Map[uintptr, struct{}])
				scene.entitiesByComponent.Set(typ, ents)
			}

			ents.Set(ptr, struct{}{})

			return true
		})
	}
}

// removeEntities removes all the given entities from the scene.
func (scene *Scene) removeEntities(entities ...*Entity) {
	log.Trace("remove %d entities", len(entities))

	length := len(scene.entities)
	for _, entity := range entities {
		ptr := (uintptr)(unsafe.Pointer(entity))
		i, ok := scene.entityIndices.Get(ptr)
		if !ok {
			continue
		}

		// remove from entities slice.
		scene.entities[i] = scene.entities[length-1]
		scene.entityIndices.Set((uintptr)(unsafe.Pointer(scene.entities[length-1])), i)
		scene.entityIndices.Delete(ptr)
		length--

		// remove from components index.
		entity.components.Scan(func(typ uintptr, _ any) bool {
			ents, ok := scene.entitiesByComponent.Get(typ)
			if ok {
				ents.Delete(ptr)
			}
			return true
		})
	}

	// trim off end.
	scene.entities = scene.entities[:length]
}
