module gitlab.com/33blue/wecqs

go 1.19

require (
	github.com/fatih/color v1.13.0
	github.com/goccy/go-json v0.9.11
	github.com/google/go-cmp v0.5.8
	github.com/hajimehoshi/ebiten/v2 v2.4.7
	github.com/tidwall/btree v1.4.4
	gitlab.com/lamados/random v0.0.0-20220807164820-a615dfe94040
	gitlab.com/lamados/slap v0.1.1
	golang.org/x/exp v0.0.0-20221012211006-4de253d81b95
	golang.org/x/image v0.0.0-20220902085622-e7cb96979f69
)

require (
	github.com/ebitengine/purego v0.1.0 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20220806181222-55e207c401ad // indirect
	github.com/hajimehoshi/file2byteslice v1.0.0 // indirect
	github.com/hajimehoshi/go-mp3 v0.3.3 // indirect
	github.com/hajimehoshi/oto/v2 v2.3.1 // indirect
	github.com/jezek/xgb v1.0.1 // indirect
	github.com/jfreymuth/oggvorbis v1.0.4 // indirect
	github.com/jfreymuth/vorbis v1.0.2 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	golang.org/x/exp/shiny v0.0.0-20221012211006-4de253d81b95 // indirect
	golang.org/x/mobile v0.0.0-20221012134814-c746ac228303 // indirect
	golang.org/x/sys v0.0.0-20221013171732-95e765b1cc43 // indirect
	golang.org/x/text v0.3.8 // indirect
	gonum.org/v1/gonum v0.11.0 // indirect
)
