package wecqs

import (
	ebitengine "github.com/hajimehoshi/ebiten/v2"
)

type WorldLoadSystem interface {
	// OnWorldLoad runs when a world is being loaded.
	OnWorldLoad(state *State, world *World) error
}

type WorldUnloadSystem interface {
	// OnWorldUnload runs when a world is being unloaded.
	OnWorldUnload(state *State, world *World) error
}

type SceneLoadSystem interface {
	// OnSceneLoad runs when a scene is being loaded.
	OnSceneLoad(state *State, scene *Scene) error
}

type SceneUnloadSystem interface {
	// OnSceneUnload runs when a scene is unloaded.
	OnSceneUnload(state *State, scene *Scene) error
}

type UpdateSystem interface {
	// OnUpdate runs every update cycle, set by the game maximum tickrate (TPS).
	OnUpdate(state *State) error
}

type DrawSystem interface {
	// OnDraw runs every draw cycle, set by the games maximum framerate (FPS).
	OnDraw(state *State, canvas *ebitengine.Image)
}

type LayoutSystem interface {
	// OnLayout runs whenever the game window is resized.
	OnLayout(state *State, canvas *ebitengine.Image)
}
