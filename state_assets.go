package wecqs

import (
	"bytes"
	"embed"
	"errors"
	"image"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"github.com/goccy/go-json"
	ebitengine "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/audio"
	"github.com/hajimehoshi/ebiten/v2/audio/mp3"
	"github.com/hajimehoshi/ebiten/v2/audio/vorbis"
	"github.com/hajimehoshi/ebiten/v2/audio/wav"
	ebitengineutil "github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
)

type AssetReader func(path string) (fs.FS, error)

// stateAssets is the state asset handler.
type stateAssets struct {
	filesystem   fs.FS
	assetReaders map[string]AssetReader
}

// TODO: move all methods here to be received by *stateAssets, then add state methods to preserve compatibility.
// TODO: configurable asset folder names maybe?
// TODO: filesystem watcher (fsnotify) for watching asset folders for updates.

// AddAssetReader adds an asset handler function to the state, allowing it to read assets from a new file type. The
// function takes a path to the asset file as the only argument and returns an fs.FS and an error.
//
// It returns the state, which makes it useful for initialising the state by chaining method calls together.
func (state *State) AddAssetReader(ext string, assetReader AssetReader) *State {
	LogDebug("add asset reader: %s", ext)

	// do nothing if blank ext provided.
	if ext == "" {
		return state
	}

	// prepend with . if not already present.
	if ext[0] != '.' {
		ext = "." + ext
	}

	// set and return.
	state.assets.assetReaders[strings.ToLower(ext)] = assetReader
	return state
}

// GetAsset reads a given asset for this world and returns its raw bytes.
func (state *State) GetAsset(name string) (raw []byte, err error) {
	LogTrace("get asset: %s", name)

	return state.read(name)
}

// GetImage returns an image with the given name.
func (state *State) GetImage(name string) (img *ebitengine.Image, i image.Image, err error) {
	LogTrace("get image: %s", name)

	f, err := state.open(filepath.Join("images", name))
	if err != nil {
		return nil, nil, err
	}
	defer func(f fs.File) {
		e := f.Close()
		if e != nil && err == nil {
			err = e
		}
	}(f)

	img, i, err = ebitengineutil.NewImageFromReader(f)
	return img, i, err
}

// GetFont returns an OpenType font with the given name.
func (state *State) GetFont(name string, size float64) (font.Face, error) {
	LogTrace("get font: %s", name)

	raw, err := state.read(filepath.Join("fonts", name))
	if err != nil {
		return nil, err
	}

	f, err := opentype.Parse(raw)
	if err != nil {
		return nil, err
	}

	return opentype.NewFace(f, &opentype.FaceOptions{Size: size, DPI: 72, Hinting: font.HintingFull})
}

// GetAudio returns an MP3, WAV, or Vorbis audio file as an io.ReadSeeker.
// Usually, NewAudioPlayer, NewLoopAudioPlayer, or NewLoopWithIntroAudioPlayer would be preferred.
func (state *State) GetAudio(name string) (s io.ReadSeeker, err error) {
	LogTrace("get audio: %s", name)

	ext := strings.ToLower(filepath.Ext(name))
	switch ext {
	case ".mp3", ".ogg", ".wav":
		f, err := state.open(filepath.Join("audio", name))
		if err != nil {
			return nil, err
		}
		defer func(f fs.File) {
			e := f.Close()
			if e != nil && err == nil {
				err = e
			}
		}(f)

		switch ext {
		case ".mp3":
			s, err = mp3.DecodeWithSampleRate(state.audio.SampleRate(), f)
		case ".ogg":
			s, err = vorbis.DecodeWithSampleRate(state.audio.SampleRate(), f)
		case ".wav":
			s, err = wav.DecodeWithSampleRate(state.audio.SampleRate(), f)
		default:
			// getting here should be impossible.
			return nil, errors.New("something has gone very wrong")
		}

		return s, err
	default:
		return nil, ErrUnhandledType
	}
}

func (state *State) GetJSON(name string, dst any) (err error) {
	LogTrace("get json: %s", name)

	f, err := state.open(name)
	if err != nil {
		return err
	}
	defer func(f fs.File) {
		e := f.Close()
		if e != nil && err == nil {
			err = e
		}
	}(f)

	return json.NewDecoder(f).Decode(dst)
}

func (state *State) NewAudioPlayer(name string) (*audio.Player, error) {
	LogTrace("new audio player: %s", name)

	src, err := state.GetAudio(name)
	if err != nil {
		return nil, err
	}

	player, err := state.audio.NewPlayer(src)
	return player, nil
}

func (state *State) NewLoopAudioPlayer(name string, length int64) (*audio.Player, error) {
	LogTrace("new loop audio player: %s", name)

	src, err := state.GetAudio(name)
	if err != nil {
		return nil, err
	}

	player, err := state.audio.NewPlayer(audio.NewInfiniteLoop(src, length))
	return player, nil
}

func (state *State) NewLoopWithIntroAudioPlayer(name string, introLength, loopLength int64) (*audio.Player, error) {
	LogTrace("new loop with intro audio player: %s", name)

	src, err := state.GetAudio(name)
	if err != nil {
		return nil, err
	}

	player, err := state.audio.NewPlayer(audio.NewInfiniteLoopWithIntro(src, introLength, loopLength))
	return player, nil
}

func (state *State) reloadAssets() (err error) {
	LogTrace("reloading assets")

	switch v := state.world.assets.(type) {
	case nil:
		state.assets.filesystem = nil
		return nil
	case string:
		// get absolute path.
		v, err = filepath.Abs(v)
		if err != nil {
			return err
		}

		// get file/folder info.
		stat, err := os.Stat(v)
		if err != nil {
			return err
		}

		// get file extension.
		ext := strings.ToLower(filepath.Ext(v))
		if ext == "" {
			// no file extension, presume it's a folder.
			if !stat.IsDir() {
				return ErrExpectedFolder
			}

			state.assets.filesystem = os.DirFS(v)
			return nil
		}

		// attempt to get asset reader for this extension.
		fn, ok := state.assets.assetReaders[ext]
		if !ok {
			// no asset reader available for this ext.
			return ErrUnhandledType
		}

		// use asset reader.
		state.assets.filesystem, err = fn(v)
		return err
	case fs.FS:
		state.assets.filesystem = v
		return nil
	case embed.FS:
		state.assets.filesystem = v
		return nil
	default:
		return ErrUnhandledType
	}
}

func (state *State) open(name string) (fs.File, error) {
	if state.assets.filesystem == nil {
		return nil, ErrAssetUnavailable
	}

	return state.assets.filesystem.Open(name)
}

func (state *State) read(name string) (raw []byte, err error) {
	f, err := state.open(name)
	if err != nil {
		return nil, err
	}
	defer func(f fs.File) {
		e := f.Close()
		if e != nil && err == nil {
			err = e
		}
	}(f)

	b := bytes.Buffer{}
	_, err = io.Copy(&b, f)
	return b.Bytes(), err
}
