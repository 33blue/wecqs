package wecqs

import (
	"bytes"
	"errors"
	"image"
	"image/png"

	ebitengine "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/audio"
)

type Game struct {
	title string
	icons []image.Image

	state *State
}

// NewGame returns a new game with the given title.
func NewGame(title string) *Game {
	return &Game{
		title: title,
		state: newState(),
	}
}

// AllowResize sets if window resizing is enabled or disabled.
//
// It returns the game, which makes it useful for initialising by chaining method calls together.
// This has no effect when running on non-desktops.
func (game *Game) AllowResize(enable bool) *Game {
	LogDebug("allow resize: %t", enable)
	game.state.settings.allowResize = enable
	return game
}

// AllowFullscreen sets if native desktop fullscreen is allowed or not. This currently only has an effect on macOS (see
// ebiten.WindowResizingModeOnlyFullscreenEnabled).
//
// It returns the game, which makes it useful for initialising by chaining method calls together.
func (game *Game) AllowFullscreen(enable bool) *Game {
	LogDebug("allow fullscreen: %t", enable)
	game.state.settings.allowFullscreen = enable
	return game
}

// AllowHighDPI sets if the game should acknowledge the high-DPI settings of the host system.
//
// It returns the game, which makes it useful for initialising by chaining method calls together.
func (game *Game) AllowHighDPI(enable bool) *Game {
	LogDebug("allow high-DPI: %t", enable)
	game.state.settings.allowHighDPI = enable
	return game
}

// SetFullscreen sets if the game should be fullscreen. This does not have an effect on macOS if fullscreen has been
// enabled natively by the desktop.
//
// It returns the game, which makes it useful for initialising by chaining method calls together.
// This has no effect when running on non-desktops.
func (game *Game) SetFullscreen(enable bool) *Game {
	game.state.SetFullscreen(enable)
	return game
}

// SetTPS sets the maximum TPS (ticks per second). By default, this is set to 60. Setting the TPS to a negative number
// results in ticks being synced to the framerate (see ebiten.SetTPS).
//
// It returns the game, which makes it useful for initialising by chaining method calls together.
func (game *Game) SetTPS(tps int) *Game {
	// prevent panics when starting up.
	if tps < -1 {
		tps = -1
	}
	game.state.settings.maxTPS = tps
	return game
}

// AddIcons adds window icons to the game. Adding multiple icons allows for the one of the closest desired size to be
// chosen automatically (see ebiten.SetWindowIcon).
//
// It returns the game, which makes it useful for initialising by chaining method calls together.
// This has no effect when running on non-desktops.
func (game *Game) AddIcons(icons ...image.Image) *Game {
	game.icons = append(game.icons, icons...)
	return game
}

// SetVsync sets if vsync is enabled or disabled.
//
// It returns the game, which makes it useful for initialising by chaining method calls together.
func (game *Game) SetVsync(enable bool) *Game {
	game.state.SetVsync(enable)
	return game
}

// SetResolution sets the canvas resolution of the game.
//
// It returns the game, which makes it useful for initialising by chaining method calls together.
// This has no effect when running on non-desktops.
func (game *Game) SetResolution(width, height int) *Game {
	LogDebug("set resolution: %dx%d", width, height)
	game.state.settings.canvasWidth = width
	game.state.settings.canvasHeight = height
	return game
}

// SetScale sets the canvas scale of the game.
//
// It returns the game, which makes it useful for initialising by chaining method calls together.
func (game *Game) SetScale(scale int) *Game {
	LogDebug("set scale: %d", scale)
	game.state.settings.canvasScale = scale
	return game
}

// SetResolutionLimits sets the resolution limits of the game window. A negative value means the size is unlimited.
//
// It returns the game, which makes it useful for initialising by chaining method calls together.
// This has no effect when running on non-desktops.
func (game *Game) SetResolutionLimits(minWidth, minHeight, maxWidth, maxHeight int) *Game {
	LogDebug("set canvas resolution limits: %dx%d to %dx%d", minWidth, minHeight, maxWidth, maxHeight)
	game.state.settings.minWidth = minWidth
	game.state.settings.minHeight = minHeight
	game.state.settings.maxWidth = maxWidth
	game.state.settings.maxHeight = maxHeight
	return game
}

// AddWorlds adds all the given worlds to the game state.
//
// It returns the game, which makes it useful for initialising by chaining method calls together.
func (game *Game) AddWorlds(worlds ...*World) *Game {
	game.state.addWorlds(worlds...)
	return game
}

// Run initialises the game with a starting world and scene and runs it.
func (game *Game) Run(worldName, sceneName string) error {
	// set default WECQS icon if none specified.
	if len(game.icons) == 0 {
		icon, err := png.Decode(bytes.NewReader(wecqsIconRaw))
		if err == nil {
			game.icons = append(game.icons, icon)
		}
	}

	// set global settings.
	ebitengine.SetWindowTitle(game.title)
	ebitengine.SetWindowIcon(game.icons)
	ebitengine.SetTPS(game.state.settings.maxTPS)
	ebitengine.SetWindowSize(
		game.state.settings.canvasWidth*game.state.settings.canvasScale,
		game.state.settings.canvasHeight*game.state.settings.canvasScale,
	)
	ebitengine.SetWindowSizeLimits(
		game.state.settings.minWidth,
		game.state.settings.minHeight,
		game.state.settings.maxWidth,
		game.state.settings.maxHeight,
	)

	// set window resizing mode.
	if game.state.settings.allowResize {
		ebitengine.SetWindowResizingMode(ebitengine.WindowResizingModeEnabled)
	} else {
		if game.state.settings.allowFullscreen {
			ebitengine.SetWindowResizingMode(ebitengine.WindowResizingModeOnlyFullscreenEnabled)
		} else {
			ebitengine.SetWindowResizingMode(ebitengine.WindowResizingModeDisabled)
		}
	}

	// create audio context.
	if game.state.audio == nil {
		game.state.audio = audio.NewContext(44100)
	}

	// switch to first world/scene.
	game.state.nextWorld = worldName
	game.state.nextScene = sceneName
	err := game.state.switchActive()
	if err != nil {
		return err
	}

	// run game.
	LogInfo("running game: %s", game.title)
	err = ebitengine.RunGame(&runner{game.state})
	if err == nil || errors.Is(err, ErrShutdown) {
		LogInfo("shutting down")

		// unload current scene.
		err = game.state.unloadScene()
		if err != nil {
			return err
		}

		// unload current world.
		return game.state.unloadWorld()
	}

	// return unexpected error.
	return err
}
