package wecqs

import (
	"testing"

	"github.com/goccy/go-json"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/tidwall/btree"
	"gitlab.com/lamados/random"
)

func TestNewScene(t *testing.T) {
	s := NewScene("scene")
	if s == nil {
		t.Error("scene is nil")
	}
}

func TestScene_Name(t *testing.T) {
	s := NewScene("scene")

	if s.Name() != s.name {
		t.Errorf("%v != %v", s.Name(), s.name)
	}
}

func TestState_NewQuery(t *testing.T) {
	src := &TestComponent{
		String: "test",
		Int:    1,
		Bool:   true,
		Struct: TestStruct{
			A: 2,
			B: 3,
			C: 4,
		},
	}

	entityA := NewEntity(src)

	state := newState()
	state.addWorlds(NewWorld("world").AddScenes(NewScene("scene", entityA)))
	state.nextWorld = "world"
	state.nextScene = "scene"
	err := state.switchActive()
	if err != nil {
		t.Fatal(err)
	}

	// start query.
	var dst *TestComponent
	query := state.NewQuery(&dst)

	// get next match.
	ok := query.Next()
	if !ok {
		t.Fatal("query stopped too early")
	}

	// get entity.
	entityB := query.Entity()

	// check entities.
	if entityA != entityB {
		t.Error("returned entity is not the same")
	}

	// check if dst was set.
	if dst == nil {
		t.Error("dst is still nil")
	}

	// check differences between dst and original component.
	if !cmp.Equal(src, dst) {
		t.Error("dst not equal to original component")
		t.Error(cmp.Diff(src, dst))
	}
}

func TestQuery_Next(t *testing.T) {
	state := newState()

	scene := NewScene("scene")

	var with, without int
	for i := 0; i < 1_000_000; i++ {
		if random.Bool() {
			scene.addEntities(NewEntity(&TestComponent{}))
			with++
		} else {
			scene.addEntities(NewEntity())
			without++
		}
	}

	state.addWorlds(NewWorld("world").AddScenes(scene))
	state.nextWorld = "world"
	state.nextScene = "scene"
	err := state.switchActive()
	if err != nil {
		t.Fatal(err)
	}

	// start query for all entities
	query := state.NewQuery()
	for query.Next() {
	}

	if query.Count() != with+without {
		t.Errorf("%d entities not accounted for", query.Count()-(with+without))
	}

	// start query for entities with the test component.
	var dst *TestComponent
	query = state.NewQuery(&dst)
	for query.Next() {
	}

	// check if all entities with the component were accounted for.
	if query.Count() != with {
		t.Errorf("%d entities with component not accounted for", with)
	}
}

func TestScene_JSON(t *testing.T) {
	sceneA := NewScene("scene")

	t.Log("adding entities")
	for i := 0; i < 100; i++ {
		if random.Bool() {
			sceneA.addEntities(NewEntity(&TestComponent{}))
		} else {
			sceneA.addEntities(NewEntity())
		}
	}

	t.Log("marshalling scene A")
	rawA, err := sceneA.MarshalJSON()
	if err != nil {
		t.Fatal(err)
	}

	t.Log(string(rawA))

	t.Log("creating scene B")
	sceneB, _ := NewSceneFromJSON(rawA)

	t.Log("comparing scene entities")
	if !cmp.Equal(sceneA.entities, sceneB.entities, cmp.AllowUnexported(Entity{}), cmpopts.IgnoreTypes(btree.Map[uintptr, any]{})) {
		t.Error("sceneA entities not equal to sceneB entities")
		t.Error(cmp.Diff(sceneA.entities, sceneB.entities))
	}

	t.Log("marshalling scene B")
	rawB, err := sceneB.MarshalJSON()
	if err != nil {
		t.Fatal(err)
	}

	t.Log(string(rawB))

	var mapA, mapB map[string]any

	t.Log("unmarshalling scene A")
	err = json.Unmarshal(rawA, &mapA)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("unmarshalling scene B")
	err = json.Unmarshal(rawB, &mapB)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("comparing scenes")
	if !cmp.Equal(mapA, mapB) {
		t.Error("sceneA not equal to sceneB")
		t.Error(cmp.Diff(mapA, mapB))
	}
}
