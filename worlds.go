package wecqs

import (
	"unsafe"

	"gitlab.com/lamados/slap"

	"gitlab.com/33blue/wecqs/internal/runtime"
)

// World is a collection of scenes and systems.
type World struct {
	name string

	assets any

	scenes  map[string]*Scene
	systems *slap.Slap[uintptr, any]
}

func NewWorld(name string) *World {
	log.Trace("new world: %s", name)

	return &World{
		name: name,

		scenes: make(map[string]*Scene),
		systems: slap.New[uintptr, any](func(system any) uintptr {
			header := (*runtime.Interface)(unsafe.Pointer(&system))
			return (uintptr)(unsafe.Pointer(header.Type))
		}),
	}
}

// Name returns the world name.
func (world *World) Name() string {
	return world.name
}

// String returns the world name.
// This is here to satisfy fmt.Stringer.
func (world *World) String() string {
	return world.name
}

// Scene gets a scene with the given name, or nil if it doesn't exist.
func (world *World) Scene(name string) *Scene {
	scene, _ := world.scenes[name]
	return scene
}

// AddScenes all the given scenes to the world.
//
// It returns the world, which makes it useful for initialising world by chaining method calls together.
func (world *World) AddScenes(scenes ...*Scene) *World {
	for _, scene := range scenes {
		log.Debug("add scene %s to world %s", scene, world)
		world.scenes[scene.name] = scene
	}

	return world
}

// AddSystems all the given systems to the world.
//
// It returns the world, which makes it useful for initialising world by chaining method calls together.
func (world *World) AddSystems(systems ...any) *World {
	for _, system := range systems {
		log.Debug("add system %T to world %s", system, world)
		world.systems.Append(system)
	}

	return world
}

// SetAssets sets the assets for this world.
// The given value can be a string, an fs.FS, or an embed.FS. If a string is provided, it must be a path to either a
// folder of assets, or an archive file containing the assets.
// By default, only .zip files are supported. Use (*State).AddAssetReader to add handlers for other archive types.
//
// It returns the world, which makes it useful for initialising world by chaining method calls together.
func (world *World) SetAssets(assets any) *World {
	world.assets = assets
	return world
}
