package wecqs

import (
	"unsafe"

	"github.com/tidwall/btree"

	"gitlab.com/33blue/wecqs/internal/runtime"
)

// Entity is a collection of components.
type Entity struct {
	components btree.Map[uintptr, any]
}

// NewEntity creates a new entity made up of the given components.
func NewEntity(components ...any) *Entity {
	entity := new(Entity)
	entity.addComponents(components...)
	return entity
}

// Component gets a component from this current and stores it in the given destination.
func (entity *Entity) Component(dst any) bool {
	if dst == nil {
		return false
	}

	// get type of underlying value.
	dstHeader := (*runtime.Interface)(unsafe.Pointer(&dst))
	dstPtrType := (*runtime.PointerType)(unsafe.Pointer(dstHeader.Type))
	src, ok := entity.components.Get(uintptr(unsafe.Pointer(dstPtrType.Elem)))
	if !ok {
		return false
	}

	// set value.
	srcHeader := (*runtime.Interface)(unsafe.Pointer(&src))
	*(*unsafe.Pointer)(dstHeader.Data) = srcHeader.Data

	return true
}

// addComponents adds the given components to this entity.
func (entity *Entity) addComponents(components ...any) {
	for _, component := range components {
		// add component to registry.
		RegisterComponent(component)

		// get type of component.
		header := (*runtime.Interface)(unsafe.Pointer(&component))
		entity.components.Set(uintptr(unsafe.Pointer(header.Type)), component)
	}
}
