package wecqs

// stateInputs is the state input handler.
type stateInputs struct {
	inputs map[string]*Input
}

// Register adds a new input mapping with the given name if it has not already been added.
// If an input mapping with this name already exists, nothing is done. This makes it good for initialising input mappings per-system.
func (inputs *stateInputs) Register(name string, newInput *Input) {
	if _, ok := inputs.inputs[name]; ok {
		return
	}

	log.Trace("register input: %s", name)

	inputs.inputs[name] = newInput
}

// Rebind changes an existing input mapping with conflict detection.
// When force is true, conflict detection is skipped, and mappings are created regardless of if they existed already.
// Error will still be returned if input mapping has rebinding disabled, regardless of the value of force.
func (inputs *stateInputs) Rebind(name string, newInput *Input, force bool) error {
	log.Trace("rebind input: %s", name)

	lastInput, ok := inputs.inputs[name]
	if lastInput.disableRebind {
		return ErrInputsRebindDisabled
	}

	// conflict detection.
	if !force {
		if !ok {
			return ErrInputsUnavailable
		}

		changedKeyPrimary := lastInput.KeyPrimary != newInput.KeyPrimary
		changedKeySecondary := lastInput.KeySecondary != newInput.KeySecondary
		changedMouseButton := lastInput.MouseButton != newInput.MouseButton
		changedGamepadButton := lastInput.GamepadButton != newInput.GamepadButton
		changedGamepadAxis := lastInput.GamepadAxis != newInput.GamepadAxis
		err := ErrInputsConflict{make(map[string]*Input)}

		for existingName, existingInput := range inputs.inputs {
			// skip conflict detection for the input mapping being changed.
			if existingName == name {
				continue
			}

			if changedKeyPrimary || changedKeySecondary {
				if existingInput.KeyPrimary == newInput.KeyPrimary ||
					existingInput.KeySecondary == newInput.KeySecondary ||
					existingInput.KeyPrimary == newInput.KeySecondary ||
					existingInput.KeySecondary == newInput.KeyPrimary {
					err.ConflictsWith[existingName] = existingInput
					continue
				}
			}
			if changedMouseButton {
				if existingInput.MouseButton == newInput.MouseButton {
					err.ConflictsWith[existingName] = existingInput
					continue
				}
			}
			if changedGamepadButton {
				if existingInput.GamepadButton == newInput.GamepadButton {
					err.ConflictsWith[existingName] = existingInput
					continue
				}
			}
			if changedGamepadAxis {
				if existingInput.GamepadAxis == newInput.GamepadAxis {
					err.ConflictsWith[existingName] = existingInput
					continue
				}
			}
		}

		if len(err.ConflictsWith) > 0 {
			return err
		}
	}

	// remove duplicated keys for primary and secondary.
	if newInput.KeyPrimary == newInput.KeySecondary {
		newInput.KeySecondary = -1
	}

	// change secondary to primary if primary is unset.
	if newInput.KeyPrimary == -1 {
		newInput.KeyPrimary = newInput.KeySecondary
		newInput.KeySecondary = -1
	}

	inputs.inputs[name] = newInput
	return nil
}

func (inputs *stateInputs) Input(name string) *Input {
	i, _ := inputs.inputs[name]
	return i
}

// RegisterInput adds a new input mapping with the given name if it has not already been added.
// If an input mapping with this name already exists, nothing is done. This makes it good for initialising input mappings per-system.
func (state *State) RegisterInput(name string, newInput *Input) {
	state.inputs.Register(name, newInput)
}

// RebindInput changes an existing input mapping with conflict detection.
// When force is true, conflict detection is skipped, and mappings are created regardless of if they existed already.
// Error will still be returned if input mapping has rebinding disabled, regardless of the value of force.
func (state *State) RebindInput(name string, newInput *Input, force bool) error {
	return state.inputs.Rebind(name, newInput, force)
}

// InputPressed returns true if the input with the given name is currently pressed.
func (state *State) InputPressed(name string) bool {
	input, ok := state.inputs.inputs[name]
	if !ok {
		return false
	}

	return input.IsPressed()
}

// InputJustPressed returns true if the input with the given name was just pressed.
func (state *State) InputJustPressed(name string) bool {
	input, ok := state.inputs.inputs[name]
	if !ok {
		return false
	}

	return input.IsJustPressed()
}

// InputJustReleased returns true if the input with the given name was just released.
func (state *State) InputJustReleased(name string) bool {
	input, ok := state.inputs.inputs[name]
	if !ok {
		return false
	}

	return input.IsJustReleased()
}

// InputValue returns the value of the input with the given name, between 0.0 and 1.0.
func (state *State) InputValue(name string) float64 {
	input, ok := state.inputs.inputs[name]
	if !ok {
		return 0.0
	}

	return input.Value()
}

// InputPressDuration returns the number of update cycles the input with the given name has been pressed for.
func (state *State) InputPressDuration(name string) int {
	input, ok := state.inputs.inputs[name]
	if !ok {
		return 0
	}

	return input.PressDuration()
}
