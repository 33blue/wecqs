package wecqs

import (
	"errors"

	"github.com/goccy/go-json"
)

// NewSceneFromJSON creates a new scene from raw JSON data.
func NewSceneFromJSON(raw []byte) (*Scene, error) {
	scene := new(Scene)
	err := scene.UnmarshalJSON(raw)
	return scene, err
}

// MarshalJSON returns the scene as raw JSON data.
func (scene *Scene) MarshalJSON() ([]byte, error) {
	if scene == nil {
		return nil, errors.New("scene is nil")
	}

	return json.Marshal(struct {
		Name     string    `json:"name"`
		Entities []*Entity `json:"entities"`
	}{
		Name:     scene.name,
		Entities: scene.entities,
	})
}

// UnmarshalJSON builds a scene from raw JSON data.
//
// The components present in the JSON data must have been previously registered to the component registry, either
// automatically through entities created with NewEntity, or manually using RegisterComponent.
func (scene *Scene) UnmarshalJSON(raw []byte) error {
	if scene == nil {
		return errors.New("scene is nil")
	}

	var alias struct {
		Name     string    `json:"name"`
		Entities []*Entity `json:"entities"`
	}

	err := json.Unmarshal(raw, &alias)
	scene.name = alias.Name
	scene.addEntities(alias.Entities...)

	return err
}
