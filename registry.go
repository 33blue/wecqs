package wecqs

import (
	"errors"
	"reflect"
	"unsafe"

	"github.com/goccy/go-json"
	"github.com/tidwall/btree"

	"gitlab.com/33blue/wecqs/internal/runtime"
)

var registry btree.Map[string, reflect.Type]

// RegisterComponent manually adds a given component type to the global component registry, used for unmarshalling an
// entity from JSON (see Entity.UnmarshalJSON).
//
// Components are usually added to the registry automatically when entities are created, and as such this is only
// required for components that haven't yet been added to an entity.
//
// If the same component type is attempted to be registered twice, RegisterComponent is a no-op.
func RegisterComponent(component any) {
	name := (*runtime.Interface)(unsafe.Pointer(&component)).Type.String()
	_, ok := registry.Get(name)
	if !ok {
		LogDebug("registered component: %s", name)
		registry.Set(name, reflect.TypeOf(component))
	}
}

// NewComponent creates a new component from the given JSON raw data.
//
// TODO: support non-pointer types for components (currently panics on reflect.New if non-pointer).
func newComponentFromJSON(name string, raw []byte) (any, error) {
	typ, ok := registry.Get(name)
	if !ok {
		return nil, errors.New("no component with name '" + name + "' is present in the registry")
	}

	// create new component.
	// TODO: don't use reflect.
	component := reflect.New(typ.Elem()).Interface()

	// unmarshal data into new component.
	err := json.Unmarshal(raw, component)
	if err != nil {
		return nil, err
	}

	// return.
	return component, nil
}
