package systems

import (
	"gitlab.com/33blue/wecqs"

	"gitlab.com/33blue/wecqs/examples/peanutmark/components"
)

type Spatial struct{}

func (system *Spatial) OnUpdate(state *wecqs.State) error {
	var spatial *components.Spatial
	query := state.NewQuery(&spatial)
	for query.Next() {
		// apply acceleration.
		spatial.Velocity.X += spatial.Acceleration.X
		spatial.Velocity.Y += spatial.Acceleration.Y

		// apply velocity.
		spatial.Position.X += spatial.Velocity.X
		spatial.Position.Y += spatial.Velocity.Y
	}

	return nil
}
