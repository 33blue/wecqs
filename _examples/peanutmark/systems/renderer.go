package systems

import (
	ebitengine "github.com/hajimehoshi/ebiten/v2"

	"gitlab.com/33blue/wecqs"

	"gitlab.com/33blue/wecqs/examples/peanutmark/components"
)

type Renderer struct{}

func (system *Renderer) OnDraw(state *wecqs.State, canvas *ebitengine.Image) {
	// draw entities.
	var spatial *components.Spatial
	var sprite *components.Sprite
	var color *components.Color
	query := state.NewQuery(&spatial, &sprite, &color)
	for query.Next() {
		// apply translation.
		var gm ebitengine.GeoM
		gm.Translate(spatial.Position.X, spatial.Position.Y)

		// apply colour.
		var cm ebitengine.ColorM
		if color.Enable {
			cm.RotateHue(color.Hue)
		}

		// draw sprite.
		canvas.DrawImage(sprite.Img, &ebitengine.DrawImageOptions{GeoM: gm, ColorM: cm})
	}
}
