package systems

import (
	"image/color"

	ebitengine "github.com/hajimehoshi/ebiten/v2"

	"gitlab.com/33blue/wecqs"
)

type Background struct {
	Color color.Color
	Path  string

	img *ebitengine.Image
}

func (system *Background) OnWorldLoad(state *wecqs.State, _ *wecqs.World) (err error) {
	if system.Path != "" {
		// load background from assets.
		system.img, _, err = state.GetImage(system.Path)
	} else {
		// generate new background from single pixel.
		system.img = ebitengine.NewImage(1, 1)
		system.img.Set(0, 0, color.White)
	}

	return err
}

func (system *Background) OnDraw(_ *wecqs.State, canvas *ebitengine.Image) {
	iw, ih := system.img.Size()
	cw, ch := canvas.Size()

	// apply scale.
	var gm ebitengine.GeoM
	gm.Scale(
		float64(cw)/float64(iw),
		float64(ch)/float64(ih),
	)

	// apply colour.
	var cm ebitengine.ColorM
	if system.Color != nil {
		r, g, b, a := system.Color.RGBA()
		cm.Scale(
			float64(r>>8)/255.0,
			float64(g>>8)/255.0,
			float64(b>>8)/255.0,
			float64(a>>8)/255.0,
		)
	}

	// draw.
	canvas.DrawImage(system.img, &ebitengine.DrawImageOptions{
		GeoM:   gm,
		ColorM: cm,
	})
}

func (system *Background) OnWorldUnload(*wecqs.State, *wecqs.World) error {
	system.img = nil
	return nil
}
