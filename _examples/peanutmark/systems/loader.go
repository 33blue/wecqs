package systems

import (
	ebitengine "github.com/hajimehoshi/ebiten/v2"

	"gitlab.com/33blue/wecqs"

	"gitlab.com/33blue/wecqs/examples/peanutmark/components"
)

type Loader struct {
	cache map[string]*ebitengine.Image
}

func (system *Loader) OnWorldLoad(state *wecqs.State, _ *wecqs.World) (err error) {
	// initialise cache.
	if system.cache == nil {
		system.cache = make(map[string]*ebitengine.Image)
	}

	// load sprite images.
	var sprite *components.Sprite
	query := state.NewQuery(&sprite)
	for query.Next() {
		if sprite.Path != "" {
			// check cache to see if image was already loaded.
			img, ok := system.cache[sprite.Path]
			if ok && img != nil {
				sprite.Img = img
				continue
			}

			// get new image and store in cache.
			sprite.Img, _, err = state.GetImage(sprite.Path)
			if err != nil {
				return err
			}

			system.cache[sprite.Path] = img
		}
	}

	return err
}

func (system *Loader) OnWorldUnload(state *wecqs.State, _ *wecqs.World) error {
	// remove cache.
	system.cache = nil

	// clear sprite images.
	var sprite *components.Sprite
	query := state.NewQuery(&sprite)
	for query.Next() {
		sprite.Img = nil
	}

	return nil
}
