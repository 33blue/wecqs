package systems

import (
	"fmt"
	"math/rand"
	"time"

	ebitengine "github.com/hajimehoshi/ebiten/v2"
	ebitengineutil "github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"

	"gitlab.com/33blue/wecqs"
	"gitlab.com/33blue/wecqs/examples/peanutmark/components"
)

type Spawner struct {
	Initial int
	Count   int

	TargetFPS float64
	lastFPS   float64

	Delta int

	SpritePath string
	img        *ebitengine.Image

	start        time.Time
	entitiesPlot *plot.Plot
	plotLines    plotter.XYs
}

func (system *Spawner) OnWorldLoad(state *wecqs.State, _ *wecqs.World) (err error) {
	state.RegisterInput("increase", wecqs.NewInput().AddKeys(ebitengine.KeyUp))
	state.RegisterInput("decrease", wecqs.NewInput().AddKeys(ebitengine.KeyDown))

	system.img, _, err = state.GetImage(system.SpritePath)
	if err != nil {
		return err
	}

	for i := 0; i < system.Initial; i++ {
		state.AddEntities(wecqs.NewEntity(
			&components.Spatial{
				Velocity:     components.Vec2{X: rand.Float64() * 8},
				Acceleration: components.Vec2{Y: 1},
			},
			&components.Sprite{Img: system.img},
			&components.Color{},
		))
	}
	system.Count = system.Initial

	system.entitiesPlot = plot.New()
	system.entitiesPlot.Title.Text = "entities over time"
	system.entitiesPlot.X.Label.Text = "seconds since start"
	system.entitiesPlot.Y.Label.Text = "entities"

	system.start = time.Now()

	return nil
}

func (system *Spawner) OnUpdate(state *wecqs.State) error {
	// automatically add/remove based on FPS.
	fps := ebitengine.ActualFPS()
	if fps != system.lastFPS {
		system.Delta = int((fps - system.TargetFPS) * 1000)
		system.lastFPS = fps
		system.plotLines = append(system.plotLines, plotter.XY{
			X: time.Since(system.start).Seconds(),
			Y: float64(system.Count),
		})
	}

	if system.Delta == 0 {
		// do nothing.
		return nil
	}

	if system.Delta > 0 {
		toAdd := system.Delta / 400
		entities := make([]*wecqs.Entity, 0, toAdd)
		for i := 0; i < toAdd; i++ {
			entities = append(entities, wecqs.NewEntity(
				&components.Spatial{
					Velocity:     components.Vec2{X: rand.Float64() * 8},
					Acceleration: components.Vec2{Y: 1},
				},
				&components.Sprite{Img: system.img},
				&components.Color{},
			))

			system.Count++
			system.Delta--
		}

		state.AddEntities(entities...)
	} else {
		toRemove := -system.Delta / 1200
		var spatial *components.Spatial
		var sprite *components.Sprite
		query := state.NewQuery(&spatial, &sprite)
		entities := make([]*wecqs.Entity, 0, toRemove)
		for i := 0; i < toRemove; i++ {
			if !query.Next() {
				break
			}

			entities = append(entities, query.Entity())
			system.Count--
			system.Delta++
		}

		state.RemoveEntities(entities...)
	}

	return nil
}

func (system *Spawner) OnDraw(_ *wecqs.State, canvas *ebitengine.Image) {
	ebitengineutil.DebugPrintAt(canvas, fmt.Sprintf(
		"FPS: %.1f\nTPS: %.1f\n\nCount: %d",
		ebitengine.ActualFPS(), ebitengine.ActualTPS(), system.Count),
		24, 24)
}

func (system *Spawner) OnWorldUnload(*wecqs.State, *wecqs.World) error {
	system.img = nil

	err := plotutil.AddLinePoints(system.entitiesPlot, "entities", system.plotLines)
	if err != nil {
		return err
	}

	return system.entitiesPlot.Save(12*vg.Inch, 4*vg.Inch, "graph.png")
}
