package systems

import (
	"gitlab.com/lamados/random"

	"gitlab.com/33blue/wecqs"

	"gitlab.com/33blue/wecqs/examples/peanutmark/components"
)

type Bounce struct{}

func (system *Bounce) OnUpdate(state *wecqs.State) error {
	cw, ch := state.CanvasSize()

	var spatial *components.Spatial
	var sprite *components.Sprite
	query := state.NewQuery(&sprite, &spatial)
	for query.Next() {
		iw, ih := sprite.Img.Size()

		if spatial.Position.X+float64(iw) > float64(cw) {
			spatial.Velocity.X *= -1
			spatial.Position.X = float64(cw) - float64(iw)
		} else if spatial.Position.X < 0 {
			spatial.Velocity.X *= -1
			spatial.Position.X = 0
		}

		if spatial.Position.Y+float64(ih) > float64(ch) {
			spatial.Velocity.Y *= random.Float(-0.98, -1)
			spatial.Position.Y = float64(ch) - float64(ih)
		} else if spatial.Position.Y < 0 {
			spatial.Velocity.Y *= -0.98
			spatial.Position.Y = 0
		}
	}

	return nil
}
