package main

import (
	_ "image/png"
	"math/rand"
	"os"
	"time"

	"golang.org/x/image/colornames"

	"gitlab.com/33blue/wecqs/examples/peanutmark/assets"
	"gitlab.com/33blue/wecqs/examples/peanutmark/systems"

	"gitlab.com/33blue/wecqs"
)

func init() {
	// seed random number generator.
	rand.Seed(time.Now().UnixNano())
}

func main() {
	// set logger.
	wecqs.SetLogger(&wecqs.SimpleLogger{Level: wecqs.SimpleLoggerLevelDebug})

	// create new game state.
	game := wecqs.NewGame("Peanutmark").
		SetResolution(800, 600).
		AllowHighDPI(false).
		SetScale(2).
		SetVsync(false).
		AllowFullscreen(true)

	// add 2D benchmark world.
	game.AddWorlds(benchmark2D())

	// run peanutmark.
	err := game.Run("2d", "peanutmark")
	if err != nil {
		wecqs.LogError("error running peanutmark: %v", err)
		os.Exit(1)
	}
}

func benchmark2D() *wecqs.World {
	// initialise 2D benchmark world.
	world := wecqs.NewWorld("2d").
		AddSystems(
			&systems.Background{Color: colornames.Grey},
			&systems.Loader{},
			&systems.Spatial{},
			&systems.Bounce{},
			&systems.Renderer{},
			&systems.Spawner{Initial: 10000, TargetFPS: 60.0, SpritePath: "peanut.png"},
		).
		AddScenes(wecqs.NewScene("peanutmark")).
		SetAssets(assets.FS)

	return world
}
