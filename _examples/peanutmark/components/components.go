package components

import (
	ebitengine "github.com/hajimehoshi/ebiten/v2"
)

type Vec2 struct {
	X, Y float64
}

type Spatial struct {
	Position     Vec2
	Velocity     Vec2
	Acceleration Vec2
}

type Color struct {
	Enable bool
	Hue    float64
}

type Sprite struct {
	Path string
	Img  *ebitengine.Image
}
