module gitlab.com/33blue/wecqs/examples

go 1.19

replace gitlab.com/33blue/wecqs => ../

require (
	github.com/fsnotify/fsnotify v1.6.0
	github.com/goccy/go-json v0.9.11
	github.com/hajimehoshi/ebiten/v2 v2.4.7
	github.com/pkg/profile v1.7.0
	gitlab.com/33blue/wecqs v0.6.0-alpha.5
	gitlab.com/lamados/random v0.0.0-20220807164820-a615dfe94040
	golang.org/x/exp v0.0.0-20221012211006-4de253d81b95
	golang.org/x/image v0.0.0-20220902085622-e7cb96979f69
	gonum.org/v1/gonum v0.11.0
	gonum.org/v1/plot v0.10.1
)

require (
	git.sr.ht/~sbinet/gg v0.3.1 // indirect
	github.com/ajstarks/svgo v0.0.0-20211024235047-1546f124cd8b // indirect
	github.com/ebitengine/purego v0.1.0 // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/felixge/fgprof v0.9.3 // indirect
	github.com/go-fonts/liberation v0.2.0 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20220806181222-55e207c401ad // indirect
	github.com/go-latex/latex v0.0.0-20210823091927-c0d11ff05a81 // indirect
	github.com/go-pdf/fpdf v0.6.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/google/pprof v0.0.0-20211214055906-6f57359322fd // indirect
	github.com/hajimehoshi/file2byteslice v1.0.0 // indirect
	github.com/hajimehoshi/go-mp3 v0.3.3 // indirect
	github.com/hajimehoshi/oto/v2 v2.3.1 // indirect
	github.com/jezek/xgb v1.0.1 // indirect
	github.com/jfreymuth/oggvorbis v1.0.4 // indirect
	github.com/jfreymuth/vorbis v1.0.2 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/tidwall/btree v1.4.4 // indirect
	gitlab.com/lamados/slap v0.1.1 // indirect
	golang.org/x/exp/shiny v0.0.0-20221012211006-4de253d81b95 // indirect
	golang.org/x/mobile v0.0.0-20221012134814-c746ac228303 // indirect
	golang.org/x/sys v0.0.0-20221013171732-95e765b1cc43 // indirect
	golang.org/x/text v0.3.8 // indirect
)
