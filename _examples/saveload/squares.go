package main

import (
	"image/color"

	ebitengine "github.com/hajimehoshi/ebiten/v2"
	ebitengineutil "github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gitlab.com/lamados/random"

	"gitlab.com/33blue/wecqs"
)

type Square struct {
	Size                 int
	PositionX, PositionY int
	VelocityX, VelocityY int
}

var (
	sizes      = []int{4, 8, 16, 32, 64}
	velocities = []int{-4, -2, -1, 1, 2, 4}
)

func NewSquare() *wecqs.Entity {
	return wecqs.NewEntity(&Square{
		Size:      random.Element(sizes),
		VelocityX: random.Element(velocities),
		VelocityY: random.Element(velocities),
	})
}

type SquareSystem struct{}

func (system *SquareSystem) OnWorldLoad(state *wecqs.State, _ *wecqs.World) error {
	state.RegisterInput("add", wecqs.NewInput().AddKeys(ebitengine.KeyUp))
	state.RegisterInput("del", wecqs.NewInput().AddKeys(ebitengine.KeyDown))

	return nil
}

func (system *SquareSystem) OnUpdate(state *wecqs.State) error {
	width, height := state.CanvasSize()

	var square *Square
	query := state.NewQuery(&square)
	for query.Next() {
		if square.PositionX < 0 {
			square.VelocityX = -square.VelocityX
			square.PositionX = 0
		}

		if square.PositionX+square.Size > width {
			square.VelocityX = -square.VelocityX
			square.PositionX = width - square.Size
		}

		square.PositionX += square.VelocityX

		if square.PositionY < 0 {
			square.VelocityY = -square.VelocityY
			square.PositionY = 0
		}

		if square.PositionY+square.Size > height {
			square.VelocityY = -square.VelocityY
			square.PositionY = height - square.Size
		}

		square.PositionY += square.VelocityY
	}

	if state.InputJustPressed("add") {
		state.AddEntities(NewSquare())
	}

	if state.InputJustPressed("del") {
		query = state.NewQuery(&square)
		if query.Next() {
			state.RemoveEntities(query.Entity())
		}
	}

	return nil
}

func (system *SquareSystem) OnDraw(state *wecqs.State, canvas *ebitengine.Image) {
	var square *Square
	query := state.NewQuery(&square)
	for query.Next() {
		ebitengineutil.DrawRect(
			canvas,
			float64(square.PositionX),
			float64(square.PositionY),
			float64(square.Size),
			float64(square.Size),
			color.NRGBA{R: 255, G: 127, B: 127, A: 127},
		)
	}
}
