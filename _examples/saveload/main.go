package main

import (
	"os"

	"gitlab.com/33blue/wecqs"
)

func main() {
	wecqs.SetLogger(&wecqs.SimpleLogger{Level: wecqs.SimpleLoggerLevelTrace})
	wecqs.RegisterComponent(&Square{})

	game := wecqs.NewGame("JSON Save/Load Test").
		AllowHighDPI(false).
		SetResolution(320, 240).
		SetScale(2).
		AllowResize(false).
		AddWorlds(
			wecqs.NewWorld("saveload").
				AddScenes(wecqs.NewScene("saveload")).
				AddSystems(&SquareSystem{}, &SaveLoadSystem{}))

	err := game.Run("saveload", "saveload")
	if err != nil {
		wecqs.LogError("game run error: %v", err)
		os.Exit(1)
	}
}
