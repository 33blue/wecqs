package main

import (
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/goccy/go-json"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"

	"gitlab.com/33blue/wecqs"
)

type SaveLoadSystem struct {
	MessageText    string
	MessageTimeout time.Time

	SaveData []byte
}

func (system *SaveLoadSystem) OnDraw(_ *wecqs.State, canvas *ebiten.Image) {
	if time.Now().Before(system.MessageTimeout) {
		ebitenutil.DebugPrint(canvas, system.MessageText)
	}
}

func (system *SaveLoadSystem) OnWorldLoad(state *wecqs.State, _ *wecqs.World) error {
	state.RegisterInput("save", wecqs.NewInput().AddKeys(ebiten.KeyF5))
	state.RegisterInput("load", wecqs.NewInput().AddKeys(ebiten.KeyF6))

	return nil
}

func (system *SaveLoadSystem) OnSceneLoad(_ *wecqs.State, scene *wecqs.Scene) error {
	raw, err := os.ReadFile("save.json")
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			system.SetMessage("new save created!")
			return nil
		}

		system.SetMessage("failed to load save!")
		wecqs.LogError(err.Error())
		return nil
	}

	err = scene.UnmarshalJSON(raw)
	if err != nil {
		system.SetMessage("failed to load save!")
		wecqs.LogError(err.Error())
		return nil
	}

	system.SetMessage("loaded save!")
	return nil
}

func (system *SaveLoadSystem) OnUpdate(state *wecqs.State) error {
	if state.InputJustPressed("save") {
		var savedata []json.RawMessage

		query := state.NewQuery()
		for query.Next() {
			raw, err := query.Entity().MarshalJSON()
			if err != nil {
				return err
			}

			savedata = append(savedata, raw)
		}

		system.SaveData, _ = json.Marshal(savedata)
		system.SetMessage("saved %d entities!", query.Count())

		return nil
	}

	if state.InputJustPressed("load") {
		if system.SaveData == nil {
			system.SetMessage("save data unavailable")
			return nil
		}

		var savedata []json.RawMessage
		err := json.Unmarshal(system.SaveData, &savedata)
		if err != nil {
			return err
		}

		query := state.NewQuery()
		for query.Next() {
			state.RemoveEntities(query.Entity())
		}

		for _, raw := range savedata {
			entity, err := wecqs.NewEntityFromJSON(raw)
			if err != nil {
				return err
			}

			state.AddEntities(entity)
		}

		system.SetMessage("loaded %d entities!", len(savedata))

		return nil
	}

	return nil
}

func (system *SaveLoadSystem) OnSceneUnload(_ *wecqs.State, scene *wecqs.Scene) error {
	raw, err := scene.MarshalJSON()
	if err != nil {
		return err
	}

	return os.WriteFile("save.json", raw, 0o600)
}

func (system *SaveLoadSystem) SetMessage(format string, a ...any) {
	system.MessageText = fmt.Sprintf(format, a...)
	system.MessageTimeout = time.Now().Add(5 * time.Second)
}
