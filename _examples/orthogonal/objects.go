package main

import (
	"image/color"
	"math"
	"math/rand"

	"github.com/goccy/go-json"
	"github.com/hajimehoshi/ebiten/v2"
	"gonum.org/v1/gonum/spatial/r2"
	"gonum.org/v1/gonum/spatial/r3"

	"gitlab.com/33blue/wecqs"
)

// Vertex represents a single point.
type Vertex struct {
	Index int         `json:"index"`
	Color color.Color `json:"color"`
	Vec   r3.Vec      `json:"vec"`
}

func (vertex *Vertex) UnmarshalJSON(raw []byte) error {
	type Alias struct {
		Color [4]uint8 `json:"color"`
		Vec   r3.Vec   `json:"vec"`
	}

	var alias Alias
	err := json.Unmarshal(raw, &alias)
	if err != nil {
		return err
	}

	vertex.Vec = alias.Vec
	vertex.Color = color.RGBA{
		R: alias.Color[0],
		G: alias.Color[1],
		B: alias.Color[2],
		A: alias.Color[3],
	}

	return nil
}

func (vertex *Vertex) MarshalJSON() ([]byte, error) {
	type Alias struct {
		Color [4]uint8 `json:"color"`
		Vec   r3.Vec   `json:"vec"`
	}

	r, g, b, a := vertex.Color.RGBA()
	alias := Alias{
		Color: [4]uint8{
			uint8(r >> 8),
			uint8(g >> 8),
			uint8(b >> 8),
			uint8(a >> 8),
		},
		Vec: vertex.Vec,
	}

	return json.Marshal(alias)
}

// Poly represents a variable number of vertices (at least 3) mapped on to a parent image
type Poly struct {
	Src     []r2.Vec `json:"src"`
	Dst     []Vertex `json:"dst"`
	Indices []uint16 `json:"indices"`
}

// SurfaceNormal calculates and returns the surface normal of this polygon using Newell's method.
// https://www.khronos.org/opengl/wiki/Calculating_a_Surface_Normal#Newell.27s_Method
func (polygon Poly) SurfaceNormal() (normal r3.Vec) {
	for i := 0; i < len(polygon.Indices); i++ {
		curr := polygon.Indices[i]
		next := polygon.Indices[(i+1)%len(polygon.Dst)]
		normal.X += (polygon.Dst[curr].Vec.Y - polygon.Dst[next].Vec.Y) * (polygon.Dst[curr].Vec.Z + polygon.Dst[next].Vec.Z)
		normal.Y += (polygon.Dst[curr].Vec.Z - polygon.Dst[next].Vec.Z) * (polygon.Dst[curr].Vec.X + polygon.Dst[next].Vec.X)
		normal.Z += (polygon.Dst[curr].Vec.X - polygon.Dst[next].Vec.X) * (polygon.Dst[curr].Vec.Y + polygon.Dst[next].Vec.Y)
	}

	// normalise.
	norm := math.Sqrt(r3.Norm2(normal))
	normal.X /= norm
	normal.Y /= norm
	normal.Z /= norm

	return normal
}

// Mesh represents a textured set of polygons.
type Mesh struct {
	Image string `json:"image"`
	Polys []Poly `json:"polys"`

	img *ebiten.Image
}

// Object represents a collection of meshes which can be translated, rotated, or scaled.
type Object struct {
	Offset r3.Vec `json:"offset"`
	Angle  r3.Vec `json:"angle"`
	Scale  r3.Vec `json:"scale"`
	Meshes []Mesh `json:"meshes"`
}

func newSquares(count int) []*wecqs.Entity {
	entities := make([]*wecqs.Entity, 0, count)

	for i := 0; i < count; i++ {
		c1, c2 := shade(), shade()
		entities = append(entities, wecqs.NewEntity(
			&Spatial{
				Position:         r3.Vec{X: exp(), Y: exp(), Z: exp()},
				Rotation:         r3.Vec{X: rand.Float64() * math.Pi * 2, Y: rand.Float64() * math.Pi * 2, Z: rand.Float64() * math.Pi * 2},
				Velocity:         r3.Vec{X: exp() * 128, Y: exp() * 128, Z: exp() * 128},
				AngularVelocity:  r3.Vec{X: (rand.Float64() - 0.5) / 10, Y: (rand.Float64() - 0.5) / 10, Z: (rand.Float64() - 0.5) / 10},
				LinearDampening:  0.999,
				AngularDampening: 0.998,
			},
			&Lifetime{Updates: rand.Intn(360) + 60},
			&Object{
				Scale: r3.Vec{X: rand.ExpFloat64(), Y: rand.ExpFloat64(), Z: rand.ExpFloat64()},
				Meshes: []Mesh{
					{
						img: pixel,
						Polys: []Poly{
							{
								Src: []r2.Vec{
									{X: 0, Y: 0},
									{X: 0, Y: 1},
									{X: 1, Y: 0},
									{X: 0, Y: 1},
								},
								Dst: []Vertex{
									{Index: 0, Color: c1, Vec: r3.Vec{X: -32, Y: 0, Z: -32}},
									{Index: 1, Color: c1, Vec: r3.Vec{X: -32, Y: 0, Z: 32}},
									{Index: 2, Color: c1, Vec: r3.Vec{X: 32, Y: 0, Z: -32}},
									{Index: 1, Color: c2, Vec: r3.Vec{X: -32, Y: 0, Z: 32}},
									{Index: 2, Color: c2, Vec: r3.Vec{X: 32, Y: 0, Z: -32}},
									{Index: 3, Color: c2, Vec: r3.Vec{X: 32, Y: 0, Z: 32}},
								},
								Indices: []uint16{0, 1, 2, 3, 4, 5},
							},
						},
					},
				},
			},
		))
	}

	return entities
}

func newWecqsLogo() *wecqs.Entity {
	return wecqs.NewEntity(
		&Spatial{
			Position:         r3.Vec{X: 256 * exp(), Y: 256 * exp(), Z: 256 * exp()},
			Rotation:         r3.Vec{X: rand.Float64() * math.Pi * 2, Y: rand.Float64() * math.Pi * 2, Z: rand.Float64() * math.Pi * 2},
			Velocity:         r3.Vec{X: exp(), Y: exp(), Z: exp()},
			Acceleration:     r3.Vec{X: (rand.Float64() - 0.5) / 10, Y: (rand.Float64() - 0.5) / 10, Z: (rand.Float64() - 0.5) / 10},
			AngularVelocity:  r3.Vec{X: (rand.Float64() - 0.5) / 10, Y: (rand.Float64() - 0.5) / 10, Z: (rand.Float64() - 0.5) / 10},
			LinearDampening:  1,
			AngularDampening: 1,
		},
		&Object{
			Scale: r3.Vec{X: 1, Y: 1, Z: 1},
			Meshes: []Mesh{
				{
					img: pixel,
					Polys: []Poly{
						// frame left.
						{
							Src: []r2.Vec{
								{X: 0, Y: 0},
								{X: 0, Y: 1},
								{X: 1, Y: 0},
								{X: 0, Y: 1},
							},
							Dst: []Vertex{
								{Index: 0, Color: color.RGBA{R: 0x22, G: 0x22, B: 0x22, A: 0xFF}, Vec: r3.Vec{X: -32, Y: 32, Z: -32}},
								{Index: 1, Color: color.RGBA{R: 0x22, G: 0x22, B: 0x22, A: 0xFF}, Vec: r3.Vec{X: -16, Y: 32, Z: -32}},
								{Index: 2, Color: color.RGBA{R: 0x22, G: 0x22, B: 0x22, A: 0xFF}, Vec: r3.Vec{X: -32, Y: -32, Z: -32}},
								{Index: 3, Color: color.RGBA{R: 0x22, G: 0x22, B: 0x22, A: 0xFF}, Vec: r3.Vec{X: -16, Y: -32, Z: -32}},
							},
							Indices: []uint16{0, 1, 2, 1, 3, 2},
						},
						// frame bottom 1.
						{
							Src: []r2.Vec{
								{X: 0, Y: 0},
								{X: 0, Y: 1},
								{X: 1, Y: 0},
								{X: 0, Y: 1},
							},
							Dst: []Vertex{
								{Index: 0, Color: color.RGBA{R: 0x44, G: 0x44, B: 0x44, A: 0xFF}, Vec: r3.Vec{X: -32, Y: -32, Z: -32}},
								{Index: 1, Color: color.RGBA{R: 0x44, G: 0x44, B: 0x44, A: 0xFF}, Vec: r3.Vec{X: -16, Y: -32, Z: -32}},
								{Index: 2, Color: color.RGBA{R: 0x44, G: 0x44, B: 0x44, A: 0xFF}, Vec: r3.Vec{X: -32, Y: -32, Z: 32}},
								{Index: 3, Color: color.RGBA{R: 0x44, G: 0x44, B: 0x44, A: 0xFF}, Vec: r3.Vec{X: -16, Y: -32, Z: 32}},
							},
							Indices: []uint16{0, 1, 2, 1, 3, 2},
						},
						// frame bottom 2.
						{
							Src: []r2.Vec{
								{X: 0, Y: 0},
								{X: 0, Y: 1},
								{X: 1, Y: 0},
								{X: 0, Y: 1},
							},
							Dst: []Vertex{
								{Index: 0, Color: color.RGBA{R: 0x44, G: 0x44, B: 0x44, A: 0xFF}, Vec: r3.Vec{X: -16, Y: -32, Z: 16}},
								{Index: 1, Color: color.RGBA{R: 0x44, G: 0x44, B: 0x44, A: 0xFF}, Vec: r3.Vec{X: 32, Y: -32, Z: 16}},
								{Index: 2, Color: color.RGBA{R: 0x44, G: 0x44, B: 0x44, A: 0xFF}, Vec: r3.Vec{X: -16, Y: -32, Z: 32}},
								{Index: 3, Color: color.RGBA{R: 0x44, G: 0x44, B: 0x44, A: 0xFF}, Vec: r3.Vec{X: 32, Y: -32, Z: 32}},
							},
							Indices: []uint16{0, 1, 2, 1, 3, 2},
						},
						// frame right.
						{
							Src: []r2.Vec{
								{X: 0, Y: 0},
								{X: 0, Y: 1},
								{X: 1, Y: 0},
								{X: 0, Y: 1},
							},
							Dst: []Vertex{
								{Index: 0, Color: color.RGBA{R: 0x66, G: 0x66, B: 0x66, A: 0xFF}, Vec: r3.Vec{X: 32, Y: 32, Z: 16}},
								{Index: 1, Color: color.RGBA{R: 0x66, G: 0x66, B: 0x66, A: 0xFF}, Vec: r3.Vec{X: 32, Y: 32, Z: 32}},
								{Index: 2, Color: color.RGBA{R: 0x66, G: 0x66, B: 0x66, A: 0xFF}, Vec: r3.Vec{X: 32, Y: -32, Z: 16}},
								{Index: 3, Color: color.RGBA{R: 0x66, G: 0x66, B: 0x66, A: 0xFF}, Vec: r3.Vec{X: 32, Y: -32, Z: 32}},
							},
							Indices: []uint16{0, 1, 2, 1, 3, 2},
						},
						// magenta-cyan face.
						{
							Src: []r2.Vec{
								{X: 0, Y: 0},
								{X: 0, Y: 1},
								{X: 1, Y: 0},
								{X: 0, Y: 1},
							},
							Dst: []Vertex{
								{Index: 0, Color: color.RGBA{R: 0xFF, G: 0x00, B: 0xFF, A: 0xFF}, Vec: r3.Vec{X: -32, Y: 16, Z: -32}},
								{Index: 1, Color: color.RGBA{R: 0x7F, G: 0x7F, B: 0xFF, A: 0xFF}, Vec: r3.Vec{X: -32, Y: 16, Z: 16}},
								{Index: 2, Color: color.RGBA{R: 0x7F, G: 0x7F, B: 0xFF, A: 0xFF}, Vec: r3.Vec{X: -32, Y: -32, Z: -32}},
								{Index: 3, Color: color.RGBA{R: 0x00, G: 0xFF, B: 0xFF, A: 0xFF}, Vec: r3.Vec{X: -32, Y: -32, Z: 16}},
							},
							Indices: []uint16{0, 1, 2, 1, 3, 2},
						},
						// cyan-yellow face.
						{
							Src: []r2.Vec{
								{X: 0, Y: 0},
								{X: 0, Y: 1},
								{X: 1, Y: 0},
								{X: 0, Y: 1},
							},
							Dst: []Vertex{
								{Index: 0, Color: color.RGBA{R: 0x7F, G: 0xFF, B: 0x7F, A: 0xFF}, Vec: r3.Vec{X: -16, Y: 16, Z: 32}},
								{Index: 1, Color: color.RGBA{R: 0xFF, G: 0xFF, B: 0x0F, A: 0xFF}, Vec: r3.Vec{X: 32, Y: 16, Z: 32}},
								{Index: 2, Color: color.RGBA{R: 0x00, G: 0xFF, B: 0xFF, A: 0xFF}, Vec: r3.Vec{X: -16, Y: -32, Z: 32}},
								{Index: 3, Color: color.RGBA{R: 0x7F, G: 0xFF, B: 0x7F, A: 0xFF}, Vec: r3.Vec{X: 32, Y: -32, Z: 32}},
							},
							Indices: []uint16{0, 1, 2, 1, 3, 2},
						},
						// magenta-yellow face.
						{
							Src: []r2.Vec{
								{X: 0, Y: 0},
								{X: 0, Y: 1},
								{X: 1, Y: 0},
								{X: 0, Y: 1},
							},
							Dst: []Vertex{
								{Index: 0, Color: color.RGBA{R: 0xFF, G: 0x00, B: 0xFF, A: 0xFF}, Vec: r3.Vec{X: -16, Y: -32, Z: -32}},
								{Index: 1, Color: color.RGBA{R: 0xFF, G: 0x7F, B: 0x7F, A: 0xFF}, Vec: r3.Vec{X: 32, Y: -32, Z: -32}},
								{Index: 2, Color: color.RGBA{R: 0xFF, G: 0x7F, B: 0x7F, A: 0xFF}, Vec: r3.Vec{X: -16, Y: -32, Z: 16}},
								{Index: 3, Color: color.RGBA{R: 0xFF, G: 0xFF, B: 0x00, A: 0xFF}, Vec: r3.Vec{X: 32, Y: -32, Z: 16}},
							},
							Indices: []uint16{0, 1, 2, 1, 3, 2},
						},
					},
				},
			},
		},
	)
}

func shade() color.Color {
	return color.Gray{Y: uint8(rand.Intn(256))}
}

func exp() float64 {
	if rand.Intn(2) == 0 {
		return -rand.ExpFloat64()
	}
	return rand.ExpFloat64()
}
