package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"golang.org/x/exp/maps"

	"gitlab.com/33blue/wecqs"
)

// LoaderSystem is an automatic mesh image loader.
type LoaderSystem struct {
	cache map[string]*ebiten.Image
}

func (system *LoaderSystem) OnWorldLoad(state *wecqs.State, _ *wecqs.World) error {
	system.cache = make(map[string]*ebiten.Image)
	return system.reload(state)
}

func (system *LoaderSystem) OnWorldUnload(state *wecqs.State, _ *wecqs.World) error {
	system.clear(state)
	return nil
}

// reload updates all meshes with the image specified by Mesh.Image, if specified.
func (system *LoaderSystem) reload(state *wecqs.State) (err error) {
	var object *Object
	query := state.NewQuery(&object)
	for query.Next() {
		for i := range object.Meshes {
			if object.Meshes[i].Image != "" {
				img, ok := system.cache[object.Meshes[i].Image]
				if !ok {
					// get image from game assets.
					img, _, err = state.GetImage(object.Meshes[i].Image)
					if err != nil {
						return err
					}

					// add to image cache.
					system.cache[object.Meshes[i].Image] = img
				}

				object.Meshes[i].Image = ""
				object.Meshes[i].img = img
			}

			// set default image if none specified.
			if object.Meshes[i].img == nil {
				object.Meshes[i].img = pixel
			}
		}
	}

	return err
}

// clear wipes the loader cache and resets all mesh images.
func (system *LoaderSystem) clear(state *wecqs.State) {
	var object *Object
	query := state.NewQuery(&object)
	for query.Next() {
		for i := range object.Meshes {
			object.Meshes[i].img = pixel
		}
	}

	// wipe cache.
	maps.Clear(system.cache)
}
