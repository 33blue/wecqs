package main

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"

	"gitlab.com/33blue/wecqs"
)

var pixel = ebiten.NewImage(1, 1)

func init() {
	pixel.Fill(color.White)
}

func main() {
	// run.
	err := newGame().Run("orthogonal", "orthogonal")
	if err != nil {
		wecqs.LogError("error during game execution: %v", err)
	}
}

func newGame() *wecqs.Game {
	wecqs.SetLogger(&wecqs.SimpleLogger{Level: wecqs.SimpleLoggerLevelTrace})

	game := wecqs.NewGame("orthogonal").
		AddWorlds(worldOrthogonal()).
		SetResolution(240, 160).
		SetScale(4).
		SetResolutionLimits(240, 160, -1, -1).
		SetVsync(true).
		AllowResize(true).
		AllowFullscreen(true).
		AllowHighDPI(false).
		SetTPS(60)

	return game
}

func worldOrthogonal() *wecqs.World {
	scene := wecqs.NewScene("orthogonal")

	// set up renderer and controller.
	renderer := &RendererSystem{Angle: DirectionNE, Pitch: 0.75, Zoom: 1, Debug: true}
	controller := &ControllerSystem{Renderer: renderer}

	// create world.
	world := wecqs.NewWorld("orthogonal").AddScenes(scene).AddSystems(
		controller,
		&LifetimeSystem{},
		&LoaderSystem{},
		renderer,
		&SpatialSystem{},
	)

	return world
}
