package main

import (
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"golang.org/x/exp/constraints"

	"gitlab.com/33blue/wecqs"
)

// ControllerSystem handles key inputs for a mesh renderer.
type ControllerSystem struct {
	Renderer *RendererSystem
}

func (system *ControllerSystem) ID() string {
	return "ControllerSystem"
}

func (system *ControllerSystem) OnWorldLoad(state *wecqs.State, _ *wecqs.World) error {
	state.RegisterInput("moveForward", wecqs.NewInput().AddKeys(ebiten.KeyW))
	state.RegisterInput("moveBackward", wecqs.NewInput().AddKeys(ebiten.KeyS))
	state.RegisterInput("moveLeft", wecqs.NewInput().AddKeys(ebiten.KeyA))
	state.RegisterInput("moveRight", wecqs.NewInput().AddKeys(ebiten.KeyD))
	state.RegisterInput("moveUp", wecqs.NewInput().AddKeys(ebiten.KeySpace))
	state.RegisterInput("moveDown", wecqs.NewInput().AddKeys(ebiten.KeyControlLeft))
	state.RegisterInput("rotateLeft", wecqs.NewInput().AddKeys(ebiten.KeyQ))
	state.RegisterInput("rotateRight", wecqs.NewInput().AddKeys(ebiten.KeyE))
	state.RegisterInput("zoomIn", wecqs.NewInput().AddKeys(ebiten.KeyEqual))
	state.RegisterInput("zoomOut", wecqs.NewInput().AddKeys(ebiten.KeyMinus))
	state.RegisterInput("addObject", wecqs.NewInput().AddKeys(ebiten.KeyUp))
	state.RegisterInput("delObject", wecqs.NewInput().AddKeys(ebiten.KeyDown))
	state.RegisterInput("toggleFullscreen", wecqs.NewInput().AddKeys(ebiten.KeyF11))

	return nil
}

func (system *ControllerSystem) OnUpdate(state *wecqs.State) error {
	moveForward := state.InputPressed("moveForward")
	moveBackward := state.InputPressed("moveBackward")
	moveLeft := state.InputPressed("moveLeft")
	moveRight := state.InputPressed("moveRight")
	moveUp := state.InputPressed("moveUp")
	moveDown := state.InputPressed("moveDown")
	rotateLeft := state.InputPressed("rotateLeft")
	rotateRight := state.InputPressed("rotateRight")
	zoomIn := state.InputPressed("zoomIn")
	zoomOut := state.InputPressed("zoomOut")

	// get sinAngle and cosAngle (used for movement relative to renderer angle).
	sinAngle, cosAngle := math.Sincos(system.Renderer.Angle)

	// apply forward/backward movement.
	if moveForward && !moveBackward {
		system.Renderer.Centre.X += sinAngle * 10 / system.Renderer.Zoom
		system.Renderer.Centre.Z += cosAngle * 10 / system.Renderer.Zoom
	} else if moveBackward && !moveForward {
		system.Renderer.Centre.X -= sinAngle * 10 / system.Renderer.Zoom
		system.Renderer.Centre.Z -= cosAngle * 10 / system.Renderer.Zoom
	}

	// apply left/right movement.
	if moveLeft && !moveRight {
		system.Renderer.Centre.X += cosAngle * 10 / system.Renderer.Zoom
		system.Renderer.Centre.Z -= sinAngle * 10 / system.Renderer.Zoom
	} else if moveRight && !moveLeft {
		system.Renderer.Centre.X -= cosAngle * 10 / system.Renderer.Zoom
		system.Renderer.Centre.Z += sinAngle * 10 / system.Renderer.Zoom
	}

	// apply up/down movement.
	if moveUp && !moveDown {
		system.Renderer.Centre.Y += 10 / system.Renderer.Zoom
	} else if moveDown && !moveUp {
		system.Renderer.Centre.Y -= 10 / system.Renderer.Zoom
	}

	// apply rotation.
	if rotateLeft && !rotateRight {
		system.Renderer.Angle -= 0.04
	} else if rotateRight && !rotateLeft {
		system.Renderer.Angle += 0.04
	}

	// apply zoom.
	if zoomIn && !zoomOut {
		system.Renderer.Zoom *= 1.02
	} else if zoomOut && !zoomIn {
		system.Renderer.Zoom *= 0.98
	}

	// keep renderer zoom within bounds [0.1, 10.0].
	system.Renderer.Zoom = clamp(0.1, system.Renderer.Zoom, 10)

	// apply pitch and angle.
	angle, pitch := ebiten.Wheel()
	system.Renderer.Angle = math.Mod(system.Renderer.Angle+(angle/(math.Pi*10)), math.Pi*2)
	system.Renderer.Pitch = clamp(0, system.Renderer.Pitch-(pitch/10), math.Pi/2)

	// add objects.
	if state.InputPressed("addObject") {
		state.AddEntities(newWecqsLogo())
	}

	// remove objects.
	if state.InputPressed("delObject") {
		var spatial *Spatial
		var object *Object
		query := state.NewQuery(&spatial, &object)
		if query.Next() {
			state.RemoveEntities(query.Entity())
		}
	}

	// toggle fullscreen
	if state.InputJustPressed("toggleFullscreen") {
		ebiten.SetFullscreen(!ebiten.IsFullscreen())
	}

	return nil
}

func clamp[T constraints.Integer | constraints.Float](minimum, preferred, maximum T) T {
	if minimum > preferred {
		return minimum
	}

	if preferred > maximum {
		return maximum
	}

	return preferred
}
