package main

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/fsnotify/fsnotify"
	"github.com/goccy/go-json"

	"gitlab.com/33blue/wecqs"
)

type ModellerSystem struct {
	Paths []string

	entities map[string]*wecqs.Entity
	watcher  *fsnotify.Watcher
	done     chan struct{}
}

func (system *ModellerSystem) OnWorldLoad(state *wecqs.State, _ *wecqs.World) (err error) {
	// initialise watcher.
	system.watcher, err = fsnotify.NewWatcher()
	if err != nil {
		return err
	}

	// add modeller paths to watcher.
	for _, path := range system.Paths {
		err = system.watcher.Add(path)
		if err != nil {
			return fmt.Errorf("add '%s': %w", path, err)
		}
	}

	// initialise entities map.
	if system.entities == nil {
		system.entities = make(map[string]*wecqs.Entity)
	}

	// initialise done channel.
	if system.done == nil {
		system.done = make(chan struct{})
	}

	go system.watch(state)

	return nil
}

func (system *ModellerSystem) OnWorldUnload(*wecqs.State, *wecqs.World) (err error) {
	system.done <- struct{}{}
	return system.watcher.Close()
}

func (system *ModellerSystem) watch(state *wecqs.State) {
	for {
		select {
		case event := <-system.watcher.Events:
			if filepath.Ext(event.Name) != ".json" {
				continue
			}

			if event.Op&fsnotify.Write == fsnotify.Write {
				entity, ok := system.entities[event.Name]
				if !ok {
					wecqs.LogInfo("creating entity '%s'", event.Name)
					entity = system.newEntity(state, event.Name)
				} else {
					wecqs.LogInfo("updating entity '%s'", event.Name)
				}

				// read new object.
				o := Object{}
				raw, err := os.ReadFile(event.Name)
				if err != nil {
					wecqs.LogError("error updating entity %s: %v", event.Name, err)
					continue
				}

				err = json.Unmarshal(raw, &o)
				if err != nil {
					wecqs.LogError("error updating entity %s: %v", event.Name, err)
					continue
				}

				// default everything to using pixel for image.
				for i := range o.Meshes {
					o.Meshes[i].img = pixel
				}

				wecqs.LogDebug("%+v", o)

				// set object to new object.
				var object *Object
				_ = entity.Component(&object)
				*object = o
			}
		case err := <-system.watcher.Errors:
			wecqs.LogError("watcher error: %v", err)
		case <-system.done:
			return
		}
	}
}

func (system *ModellerSystem) newEntity(state *wecqs.State, name string) *wecqs.Entity {
	// create entity.
	entity := wecqs.NewEntity(&Object{})
	state.AddEntities(entity)
	system.entities[name] = entity
	return entity
}
