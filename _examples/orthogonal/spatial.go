package main

import (
	"gonum.org/v1/gonum/spatial/r3"

	"gitlab.com/33blue/wecqs"
)

type Spatial struct {
	Position        r3.Vec
	Rotation        r3.Vec
	Velocity        r3.Vec
	Acceleration    r3.Vec
	AngularVelocity r3.Vec

	LinearDampening  float64
	AngularDampening float64
}

type SpatialSystem struct{}

func (system *SpatialSystem) OnUpdate(state *wecqs.State) error {
	var spatial *Spatial
	query := state.NewQuery(&spatial)
	for query.Next() {
		spatial.Position = r3.Add(spatial.Position, spatial.Velocity)
		spatial.Rotation = r3.Add(spatial.Rotation, spatial.AngularVelocity)

		// update spatial.
		spatial.Velocity = r3.Scale(spatial.LinearDampening, r3.Add(spatial.Velocity, spatial.Acceleration))
		spatial.AngularVelocity = r3.Scale(spatial.AngularDampening, spatial.AngularVelocity)
	}

	return nil
}
