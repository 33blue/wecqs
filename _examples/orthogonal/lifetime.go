package main

import (
	"gitlab.com/33blue/wecqs"
)

type Lifetime struct {
	Updates int
}

type LifetimeSystem struct{}

func (system *LifetimeSystem) OnUpdate(state *wecqs.State) error {
	var lifetime *Lifetime

	query := state.NewQuery(&lifetime)
	for query.Next() {
		lifetime.Updates--

		// remove entity if expired.
		if lifetime.Updates < 0 {
			state.RemoveEntities(query.Entity())
		}
	}

	return nil
}
