package main

import (
	"math"
	"sort"
	"strconv"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gonum.org/v1/gonum/num/quat"
	"gonum.org/v1/gonum/spatial/r3"

	"gitlab.com/33blue/wecqs"
)

const (
	DirectionW = math.Pi * float64(iota) / 4
	DirectionNW
	DirectionN
	DirectionNE
	DirectionE
	DirectionSE
	DirectionS
	DirectionSW
)

// RendererSystem is an orthogonal mesh renderer.
type RendererSystem struct {
	Centre r3.Vec
	Angle  float64
	Pitch  float64
	Zoom   float64
	Debug  bool

	polygons []renderPolygon
	drawn    bool
}

type renderPolygon struct {
	Image    *ebiten.Image   `json:"image"`
	Vertices []ebiten.Vertex `json:"vertices"`
	Indices  []uint16        `json:"indices"`
	Order    float64         `json:"order"`
}

func (system *RendererSystem) OnUpdate(state *wecqs.State) error {
	// get transform function for the current renderer parameters.
	transform, sinAngle, cosAngle, sinPitch, _ := system.transformFunc()

	// iterate over results.
	count := 0
	var object *Object
	query := state.NewQuery(&object)
	for query.Next() {
		var rotation r3.Rotation
		var offset r3.Vec

		var spatial *Spatial
		ok := query.Entity().Component(&spatial)
		if ok {
			rotation = newRotation(object.Angle.X+spatial.Rotation.X, object.Angle.Y+spatial.Rotation.Y, object.Angle.Z+spatial.Rotation.Z)
			offset.X += spatial.Position.X
			offset.Y += spatial.Position.Y
			offset.Z += spatial.Position.Z
		} else {
			rotation = newRotation(object.Angle.X, object.Angle.Y, object.Angle.Z)
		}

		// do transformations and rendering for meshes.
		for i := range object.Meshes {
			// do transformations for all Polys.
			for j := range object.Meshes[i].Polys {
				// add new polygon if required.
				if len(system.polygons) <= count {
					system.polygons = append(system.polygons, renderPolygon{
						Image:    object.Meshes[i].img,
						Vertices: make([]ebiten.Vertex, len(object.Meshes[i].Polys[j].Dst)),
						Indices:  object.Meshes[i].Polys[j].Indices,
					})
				}

				// add new vertices if needed.
				if len(system.polygons[count].Vertices) < len(object.Meshes[i].Polys[j].Dst) {
					system.polygons[count].Vertices = make([]ebiten.Vertex, len(object.Meshes[i].Polys[j].Dst))
				}

				// reset order.
				system.polygons[count].Order = 0

				// set up vertices.
				for _, index := range object.Meshes[i].Polys[j].Indices {
					// do coordinate transformation on vertex.
					src := rotation.Rotate(r3.Vec{
						X: -object.Meshes[i].Polys[j].Dst[index].Vec.X * object.Scale.X,
						Y: -object.Meshes[i].Polys[j].Dst[index].Vec.Y * object.Scale.Y,
						Z: -object.Meshes[i].Polys[j].Dst[index].Vec.Z * object.Scale.Z,
					})
					src.X += object.Offset.X + offset.X
					src.Y += object.Offset.Y + offset.Y
					src.Z += object.Offset.Z + offset.Z
					transform(&src, &system.polygons[count].Vertices[index])

					// set source and destination coordinates.
					system.polygons[count].Vertices[index].SrcX = float32(object.Meshes[i].Polys[j].Src[object.Meshes[i].Polys[j].Dst[index].Index].X)
					system.polygons[count].Vertices[index].SrcY = float32(object.Meshes[i].Polys[j].Src[object.Meshes[i].Polys[j].Dst[index].Index].Y)

					// set vertex colour.
					cR, cG, cB, cA := object.Meshes[i].Polys[j].Dst[index].Color.RGBA()
					system.polygons[count].Vertices[index].ColorR = float32(cR>>8) / 255
					system.polygons[count].Vertices[index].ColorG = float32(cG>>8) / 255
					system.polygons[count].Vertices[index].ColorB = float32(cB>>8) / 255
					system.polygons[count].Vertices[index].ColorA = float32(cA>>8) / 255

					// update polygon order.
					system.polygons[count].Order -= (sinAngle * src.X) + (cosAngle * src.Z) - (src.Y * sinPitch)
				}

				// trim vertices.
				system.polygons[count].Vertices = system.polygons[count].Vertices[:len(object.Meshes[i].Polys[j].Dst)]

				count++
			}
		}
	}

	// trim polygons.
	system.polygons = system.polygons[:count]

	// sort polygons based on order.
	sort.Slice(system.polygons, func(i, j int) bool {
		return system.polygons[i].Order > system.polygons[j].Order
	})

	// reset drawn flag.
	system.drawn = false

	return nil
}

func (system *RendererSystem) OnDraw(_ *wecqs.State, canvas *ebiten.Image) {
	width, height := canvas.Size()

	// get zoom ratio (100% at 1080p, 133% at 1440p, etc).
	minSideLength := float32(height)
	if height > width {
		minSideLength = float32(width)
	}
	ratio := minSideLength / 1080.0

	// get middle of screen.
	offsetX := float32(width / 2)
	offsetY := float32(height / 2)

	// render polygons.
	for i := range system.polygons {
		// don't re-add offsets if an OnUpdate cycle hasn't run yet (prevents flickering).
		if !system.drawn {
			for j := range system.polygons[i].Vertices {
				system.polygons[i].Vertices[j].DstX = (system.polygons[i].Vertices[j].DstX * ratio) + offsetX
				system.polygons[i].Vertices[j].DstY = (system.polygons[i].Vertices[j].DstY * ratio) + offsetY
			}
		}

		canvas.DrawTriangles(system.polygons[i].Vertices, system.polygons[i].Indices, system.polygons[i].Image, nil)
	}

	// set drawn flag.
	system.drawn = true

	// debug info.
	if system.Debug {
		ebitenutil.DebugPrint(canvas,
			"FPS: "+strconv.FormatFloat(ebiten.ActualFPS(), 'f', 1, 64)+
				"\nTPS: "+strconv.FormatFloat(ebiten.ActualTPS(), 'f', 1, 64)+
				"\nXYZ: ("+strconv.FormatFloat(system.Centre.X, 'f', 1, 64)+", "+
				strconv.FormatFloat(system.Centre.Y, 'f', 1, 64)+", "+
				strconv.FormatFloat(system.Centre.Z, 'f', 1, 64)+")",
		)
	}
}

// transformFunc returns a function used to transform a 3D vector into an Ebitengine vertex, using the current renderer
// parameters (centre, angle, pitch, zoom).
func (system *RendererSystem) transformFunc() (transform func(*r3.Vec, *ebiten.Vertex), sinAngle, cosAngle, sinPitch, cosPitch float64) {
	sinAngle, cosAngle = math.Sincos(system.Angle)
	sinPitch, cosPitch = math.Sincos(system.Pitch)
	centre := system.Centre
	zoom := system.Zoom

	return func(src *r3.Vec, dst *ebiten.Vertex) {
		x := (src.X + centre.X) * zoom
		y := (src.Y + centre.Y) * zoom
		z := (src.Z + centre.Z) * zoom

		dst.DstX = float32((x * cosAngle) - (z * sinAngle))
		dst.DstY = float32((sinPitch * ((x * sinAngle) + (z * cosAngle))) + (y * cosPitch))
	}, sinAngle, cosAngle, sinPitch, cosPitch
}

func newRotation(x, y, z float64) r3.Rotation {
	var rot1, rot2, rot3 quat.Number
	rot1.Imag, rot1.Real = math.Sincos(x / 2)
	rot2.Jmag, rot2.Real = math.Sincos(y / 2)
	rot3.Kmag, rot3.Real = math.Sincos(z / 2)

	return r3.Rotation(quat.Mul(rot3, quat.Mul(rot2, rot1)))
}
