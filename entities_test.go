package wecqs

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

type TestComponent struct {
	String string
	Int    int
	Bool   bool
	Struct TestStruct
}

type TestStruct struct {
	A, B, C float64
}

func TestNewEntity(t *testing.T) {
	entity := NewEntity()
	if entity == nil {
		t.Error("entity is nil")
	}
}

func TestEntity_Components(t *testing.T) {
	entity := NewEntity()

	if entity.Component(nil) {
		t.Errorf("returned true for checking nil component")
	}

	var dst *TestComponent

	ok := entity.Component(&dst)
	if ok {
		t.Error("entity.Component returned true, should have returned false")
	}

	src := &TestComponent{
		String: "test",
		Int:    1,
		Bool:   true,
		Struct: TestStruct{
			A: 2,
			B: 3,
			C: 4,
		},
	}
	entity.addComponents(src)

	ok = entity.Component(&dst)
	if !ok {
		t.Error("entity.Component returned false, should have returned true")
	}

	if dst == nil {
		t.Errorf("dst is still nil")
	}

	entity.addComponents(src)

	if !cmp.Equal(src, dst) {
		t.Error("dst not equal to original component")
		t.Errorf(cmp.Diff(src, dst))
	}
}

func TestEntity_JSON(t *testing.T) {
	entityA := NewEntity(&TestComponent{
		String: "test",
		Int:    1,
		Bool:   true,
		Struct: TestStruct{
			A: 2,
			B: 3,
			C: 4,
		},
	})

	rawA, err := entityA.MarshalJSON()
	if err != nil {
		t.Fatal(err)
	}

	entityB, err := NewEntityFromJSON(rawA)
	if err != nil {
		t.Fatal(err)
	}

	if !cmp.Equal(entityA.components.Values(), entityB.components.Values()) {
		t.Error("entityA components not equal to entityB")
		t.Log(cmp.Diff(entityA.components.Values(), entityB.components.Values()))
	}
}
