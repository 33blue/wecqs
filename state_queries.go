package wecqs

import (
	"context"
	"unsafe"

	"gitlab.com/33blue/wecqs/internal/runtime"
)

// Query is an iterator providing entities that matched a given query in a scene.
type Query struct {
	current *Entity
	next    chan *Entity

	// destination and type pointers
	ptrs []*unsafe.Pointer
	typs []uintptr

	// entity count.
	count int

	// parent state and scene.
	state *State
	scene *Scene
}

// NewQuery sets up a query on the current scene to return entities matching the given components. If no components are
// provided, all entities will be returned.
//
//	var object *Object
//	var spatial *Spatial
//	query := state.NewQuery(&object, &spatial)
//	defer query.Cancel()
//
//	for query.Next() { /*...*/ }
//
// Entities are returned in an indeterminate order.
func (state *State) NewQuery(dsts ...any) *Query {
	// TODO: is there a smart way of adjusting this on-the-fly?
	const queryNextBufferSize = 1024

	// set up destination and type pointers.
	typs := make([]uintptr, 0, len(dsts))
	ptrs := make([]*unsafe.Pointer, 0, len(dsts))
	for _, dst := range dsts {
		iface := (*runtime.Interface)(unsafe.Pointer(&dst))
		ptrType := (*runtime.PointerType)(unsafe.Pointer(iface.Type))
		typs = append(typs, uintptr(unsafe.Pointer(ptrType.Elem)))
		ptrs = append(ptrs, (*unsafe.Pointer)(iface.Data))
	}

	// create results struct.
	query := &Query{
		next:  make(chan *Entity, queryNextBufferSize),
		ptrs:  ptrs,
		typs:  typs,
		state: state,
	}

	// set start and cancel functions.
	ctx, cancel := context.WithCancel(context.Background())
	state.cancels = append(state.cancels, cancel)
	go query.run(ctx)

	// return.
	return query
}

// Next requests the next entity matching the query.
//
// Next should never be called after Cancel is called, as it may result in inconsistent behaviour.
func (query *Query) Next() (ok bool) {
	// request next entity.
	query.current, ok = <-query.next
	if !ok {
		// channel closed.
		return false
	}
	query.count++

	// set destinations.
	for i, dst := range query.ptrs {
		src, _ := query.current.components.Get(query.typs[i])
		*dst = (*runtime.Interface)(unsafe.Pointer(&src)).Data
	}

	return true
}

// Entity returns the last matched entity.
func (query *Query) Entity() *Entity {
	return query.current
}

// Count returns the number of entities that have been returned by the query in total.
func (query *Query) Count() int {
	return query.count
}

func (query *Query) run(ctx context.Context) {
	query.state.queries.Add(1)

	go func(scene *Scene, query *Query) {
		defer query.state.queries.Done()
		defer close(query.next)

		if len(query.typs) == 0 {
			// loop over all entities.
			for _, entity := range scene.entities {
				select {
				case query.next <- entity:
				case <-ctx.Done():
					return
				}
			}
		} else {
			// get set of entities matching first component.
			entities, ok := scene.entitiesByComponent.Get(query.typs[0])
			if !ok {
				return
			}

			// loop over entities.
			entities.Scan(func(ptr uintptr, _ struct{}) bool {
				// TODO: write internal alternative to btree.Map which uses unsafe.Pointer as the key type.
				//goland:noinspection GoVetUnsafePointer
				entity := (*Entity)(unsafe.Pointer(ptr))

				// check to see if entity has all other components requested.
				for _, typ := range query.typs[1:] {
					if _, ok := entity.components.Get(typ); !ok {
						return true
					}
				}

				select {
				case query.next <- entity:
					return true
				case <-ctx.Done():
					return false
				}
			})
		}
	}(query.state.scene, query)
}
