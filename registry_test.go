package wecqs

import (
	"testing"
)

func BenchmarkRegistry_Register(b *testing.B) {
	component := new(TestComponent)
	for i := 0; i < b.N; i++ {
		RegisterComponent(component)
	}
}
