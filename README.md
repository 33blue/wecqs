# ARCHIVED: No longer in active development

I've reshifted by focus from pure engine development back to _actual game development_ for the time being. My current main project features its own ECS-like framework just to simplify things, so rather than trying to make something fairly general-purpose I will instead be able to make something specific to that project. **WECQS** may be picked up again in future as it needs a full rewrite to be properly usable by anyone, and this rewrite may possibly be forked from the framework I'm building out for this other project. But for now this repo will be archived.

Original README follows below.

---

# WECQS

<img src="/wecqs.svg" alt="WECQS logo" width="192" align="right">

**WECQS** (/wɛks/) is an experimental ECS library built on top of [Ebitengine](https://ebiten.org/), designed to be both
simple to use and extremely fast.
It achieves this by making heavy use of `unsafe.Pointer` and various other reflect-like hacks of the Go runtime, without
directly using reflect. The drawbacks here are there are no guarantees of safety, portability, or compatibility with
past or future versions of Go. As such, **WECQS currently only supports Go 1.19**.

Take a look at these other ECS packages, which may fit your use-case better and may also less likely to randomly break:

- [Artem Sedykh / **Mizu**](https://github.com/sedyh/mizu)
- [Yota Hamada / **Donburi**](https://github.com/yohamta/donburi)

## License

The `wecqs` package is distributed under the [MIT License](/LICENSE.txt).
See [this page](https://opensource.org/licenses/MIT) for more details.
