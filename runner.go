/*
Package wecqs is an ECS framework for Go game development.
*/
package wecqs

import (
	_ "embed"

	ebitengine "github.com/hajimehoshi/ebiten/v2"
)

//go:embed wecqs.png
var wecqsIconRaw []byte

// runner is a small wrapper around a *State which implements the ebitengine.Game.
type runner struct{ *State }

func (r *runner) Update() error {
	return r.update()
}

func (r *runner) Draw(screen *ebitengine.Image) {
	r.draw(screen)
}

func (r *runner) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return r.layout(outsideWidth, outsideHeight)
}
