package wecqs

import (
	"testing"
)

func TestNewWorld(t *testing.T) {
	w := NewWorld("test")
	if name := w.Name(); name != "test" {
		t.Errorf("name is '%s', not 'test'", name)
	}

	if l := w.systems.Length(); l != 0 {
		t.Errorf("should have 0 systems, has %d", l)
	}

	if len(w.scenes) != 0 {
		t.Errorf("should have 0 scenes, has %d", len(w.scenes))
	}
}

func TestWorld_AddSystems(t *testing.T) {
	w := NewWorld("test")

	s := new(TestSystem)
	w.AddSystems(s)

	if l := w.systems.Length(); l != 1 {
		t.Errorf("should have 1 system, has %d", l)
	}

	if !w.systems.Contains(s) {
		t.Errorf("does not contain %T", s)
	}
}
