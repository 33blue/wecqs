package wecqs

import (
	"testing"
	"time"

	ebitengine "github.com/hajimehoshi/ebiten/v2"
)

type TestSystem struct {
	Start    time.Time
	Duration time.Duration
	Test     *testing.T
}

func (system *TestSystem) OnWorldLoad(state *State, _ *World) error {
	system.Test.Log("OnWorldLoad")

	system.Start = time.Now()
	query := state.NewQuery()
	for query.Next() {
	}

	system.Test.Logf("%d entities", query.Count())

	return nil
}

func (system *TestSystem) OnWorldUnload(state *State, _ *World) error {
	system.Test.Log("OnWorldUnload")

	query := state.NewQuery()
	for query.Next() {
	}

	system.Test.Logf("%d entities", query.Count())

	return nil
}

func (system *TestSystem) OnSceneLoad(state *State, _ *Scene) error {
	system.Test.Log("OnSceneLoad")

	query := state.NewQuery()
	for query.Next() {
	}

	system.Test.Logf("%d entities", query.Count())

	return nil
}

func (system *TestSystem) OnSceneUnload(state *State, _ *Scene) error {
	system.Test.Log("OnSceneUnload")

	query := state.NewQuery()
	for query.Next() {
	}

	system.Test.Logf("%d entities", query.Count())

	return nil
}

func (system *TestSystem) OnUpdate(state *State) error {
	system.Test.Log("OnUpdate")

	query := state.NewQuery()
	for query.Next() {
	}

	system.Test.Logf("%d entities", query.Count())

	if time.Now().After(system.Start.Add(system.Duration)) {
		system.Test.Log("done")
		return ErrShutdown
	}

	return nil
}

func (system *TestSystem) OnDraw(state *State, _ *ebitengine.Image) {
	system.Test.Log("OnDraw")

	query := state.NewQuery()
	for query.Next() {
	}

	system.Test.Logf("%d entities", query.Count())
}

func (system *TestSystem) OnLayout(state *State, _ *ebitengine.Image) {
	system.Test.Log("OnLayout")

	query := state.NewQuery()
	for query.Next() {
	}

	system.Test.Logf("%d entities", query.Count())
}

// TestSystemQueries ensures that queries can be run from any system type without panics.
func TestSystemQueries(t *testing.T) {
	SetLogger(&SimpleLogger{Level: SimpleLoggerLevelTrace})

	game := NewGame("test").
		AddWorlds(
			NewWorld("test").
				AddSystems(&TestSystem{Test: t, Duration: 5 * time.Second}).
				AddScenes(NewScene("test", NewEntity(), NewEntity(), NewEntity())))

	go func() {
		<-time.After(10 * time.Second)
		t.Error("test timed out")
	}()

	game.state.nextWorld = "test"
	game.state.nextScene = "test"

	err := game.state.switchActive()
	if err != nil {
		t.Fatal(err)
	}

	err = game.state.update()
	if err != nil {
		t.Fatal(err)
	}

	game.state.settings.canvasWidth, game.state.settings.canvasHeight = 640, 480
	game.state.canvas = ebitengine.NewImage(game.state.settings.canvasWidth, game.state.settings.canvasHeight)
	game.state.draw(ebitengine.NewImage(game.state.settings.canvasWidth, game.state.settings.canvasHeight))

	_, _ = game.state.layout(1, 1)

	err = game.state.unloadWorld()
	if err != nil {
		t.Fatal(err)
	}

	err = game.state.unloadScene()
	if err != nil {
		t.Fatal(err)
	}
}
